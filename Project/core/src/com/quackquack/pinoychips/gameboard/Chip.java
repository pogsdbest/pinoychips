package com.quackquack.pinoychips.gameboard;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Cubic;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.utils.ActorTweenAccessor;
import com.game.framework.utils.L;
import com.quackquack.pinoychips.Assets;

public class Chip extends DisplayObject {
	
	private Type type;
	private int number;
	private int row;
	private int col;
	public boolean isSelected;
	public boolean isAvailable;
	private ChipListener pushListener;
	public boolean isOut;
	private DisplayText text;
	public boolean nope;
	
	public enum Type {
		BLACK,WHITE
	}
	
	public enum Tap {
		SELECTED,MOVE
	}
	
	public enum Neighbors {
		EAST,WEST,NORTH_EAST,NORTH_WEST,SOUTH_EAST,SOUTH_WEST
	}
	
	public Chip(TextureRegion texture,Type type,BitmapFont font,int number) {
		this(texture, type, number);
		
		text = new DisplayText(""+number, font);
		text.setAlignment(HAlignment.CENTER);
		text.autoResize = false;
		text.setPreferedWidth(getWidth());
		text.setY(15);
		//text.isDebug = true;
		text.setColor(font.getColor());
		addActor(text);
		
	}
	
	public Chip(TextureRegion texture,Type type,int number) {
		super(texture);
		this.type = type;
		this.setNumber(number);
		

	}
	
	public Type getType () {
		return this.type;
	}
	
	public void setLocation(int row,int col) {
		this.row = row;
		this.col = col;
	}
	
	public int getRow () {
		return this.row;
	}
	
	public int getColumn () {
		return this.col;
	}
	
	public void tap (Tap tap) {
		if(tap == Tap.SELECTED) {
			Assets.getInstance().playSound("sfx/touchchip.mp3");
			if(isSelected) {
				isSelected = false;
				return;
			}
			isSelected = true;
			selectedTween();
		}
	}
	
	public int getUpperLeftColumn() {
		return row%2 == 0 ? col-1 : col;
	}
	
	public int getUpperRightColumn() {
		return row%2 == 0 ? col : col+1;
	}
	
	public int getLowerLeftColumn() {
		return row%2 == 0 ? col -1 : col;
	}
	
	public int getLowerRightColumn() {
		return row%2 == 0 ? col : col+1;
	}

	private void selectedTween() {
		if(nope) return;
		nope = true;
		Tween.to(this, ActorTweenAccessor.SCALE_XY, .3f)
		.target(.75f,.75f)
	    .ease(Cubic.IN)
	    .repeatYoyo(1, 0f)
	    .start(getTweenManager())
		.setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				nope = false;
			}
		});
	}
	
	public void moveTween(final Slot slot,final MoveCallback callback) {
		Tween.to(this, ActorTweenAccessor.POSITION_XY, .5f)
		.target(slot.getX(),slot.getY())
	    .ease(Cubic.IN)
	    .start(getTweenManager())
	    .setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				setLocation(slot.getRow(), slot.getColumn());
				if(callback!=null) {
					callback.done();
				}
			}
		});
	}
	
	public void outTween(final MoveCallback callback) {
		Tween.to(this, ActorTweenAccessor.SCALE_XY, .3f)
		.target(0f,0f)
	    .ease(Cubic.IN)
	    .start(getTweenManager())
	    .setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				remove();
				if(callback!=null) callback.done();
			}
		});
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		if(isSelected) {
			batch.end();
			debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
			//change highlight color for selected chip
			debugRenderer.begin(ShapeType.Line);
			debugRenderer.setColor(Color.GREEN);
			debugRenderer.circle(getX()+getOriginX()+getParent().getX(), getY()+getOriginY()+getParent().getY(), (getWidth()/2) * getScaleX());
			debugRenderer.end();
			batch.begin();
		} else if(isAvailable) {
			batch.end();
			debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
			//change color for possible move
			debugRenderer.begin(ShapeType.Line);
			debugRenderer.setColor(Color.BLUE);
			debugRenderer.circle(getX()+getOriginX()+getParent().getX(), getY()+getOriginY()+getParent().getY(), (getWidth()/2) * getScaleX());
			debugRenderer.end();
			batch.begin();
		}
		
	}
	
	public void setPushListener(ChipListener listener) {
		if(pushListener!=null) {
			removePushListener();
		}
		this.pushListener = listener;
		addListener(listener);
	}
	
	public void removePushListener() {
		removeListener(pushListener);
		pushListener = null;
	}
	
	public ChipListener getPushListener() {
		return pushListener;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	public DisplayText getTextDisplay() {
		return text;
	}

}
