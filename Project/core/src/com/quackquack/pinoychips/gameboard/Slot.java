package com.quackquack.pinoychips.gameboard;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.game.framework.display.DisplayObject;
import com.game.framework.utils.L;

public class Slot extends DisplayObject {
	
	private int row;
	private int col;
	private boolean available;
	private ChipListener chipListener;
	
	public Slot(TextureRegion texture,int row,int col) {
		super(texture);
		this.col = col;
		this.row = row;
	}
	
	public int getRow() {
		return row;
	}
	
	public int getColumn() {
		return col;
	}
	
	public int getUpperLeftColumn() {
		return row%2 == 0 ? col-1 : col;
	}
	
	public int getUpperRightColumn() {
		return row%2 == 0 ? col : col+1;
	}
	
	public int getLowerLeftColumn() {
		return row%2 == 0 ? col -1 : col;
	}
	
	public int getLowerRightColumn() {
		return row%2 == 0 ? col : col+1;
	}
	
	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		if(available) {
			batch.end();
			debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
			debugRenderer.begin(ShapeType.Line);
			debugRenderer.setColor(Color.BLUE);
			debugRenderer.circle(getX()+getOriginX()+getParent().getX(), getY()+getOriginY()+getParent().getY(), (getWidth()/2) * getScaleX());
			debugRenderer.end();
			batch.begin();
		}
	}
	
	public void setChipListener(ChipListener listener) {
		if(chipListener==null) {
			this.chipListener = listener;
		}
		else {
			L.e("There is already a chip listener remove it first");
			return;
		}
		addListener(chipListener);
	}
	
	public void removeChipListener() {
		
		removeListener(this.chipListener);
		chipListener = null;
	}
	
	public boolean isAvailable() {
		return available;
	}
}
