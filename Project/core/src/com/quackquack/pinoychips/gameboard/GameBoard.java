package com.quackquack.pinoychips.gameboard;

import java.util.ArrayList;

import sun.font.LayoutPathImpl.EndType;
import sun.tools.jar.Main;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.game.framework.display.DisplayText;
import com.game.framework.display.ParticleDisplay;
import com.game.framework.display.ui.NinePatchDisplay;
import com.game.framework.listeners.ActorDragListener;
import com.game.framework.listeners.BackKeyListener;
import com.game.framework.listeners.ParticleDisplayListener;
import com.game.framework.utils.BackKeyCatcher;
import com.game.framework.utils.L;
import com.quackquack.pinoychips.Assets;
import com.quackquack.pinoychips.gameboard.Chip.Neighbors;
import com.quackquack.pinoychips.gameboard.Chip.Tap;
import com.quackquack.pinoychips.screen.GameScreen;
import com.quackquack.pinoychips.screen.MainMenuScreen;
import com.quackquack.pinoychips.screen.PreGameScreen;
import com.quackquack.pinoychips.screen.TutorialScreen;

public class GameBoard extends Group implements MoveCallback {
	
	public static final int BOARD_MAX_ROW = 11;
	public static final int BOARD_MAX_COLUMN = 11;
	
	private Slot[][] slots;
	
	/* _ = disable
	 * + = empty
	 * X = black
	 * O = white
	 */
	private String[][] board0 = {
			{"_","_","_","_","_","_","_","_","_","_","_"},
			{"_","_","_","+","+","+","+","+","_","_","_"},
			{"_","_","_","+","O","O","O","+","+","_","_"},
			{"_","_","+","+","+","+","+","+","+","_","_"},
			{"_","_","+","O","+","+","X","+","+","+","_"},
			{"_","+","+","O","+","+","X","+","+","+","_"},
			{"_","_","+","+","O","+","+","X","+","+","_"},
			{"_","_","+","+","+","+","+","+","+","_","_"},
			{"_","_","_","+","X","X","X","+","+","_","_"},
			{"_","_","_","+","+","+","+","+","_","_","_"},
			{"_","_","_","_","_","_","_","_","_","_","_"}
	};
	private String[][] board01 = {
			{"_","_","_","_","_","_","_","_","_","_","_"},
			{"_","_","_","+","+","+","+","+","_","_","_"},
			{"_","_","_","+","+","+","+","+","+","_","_"},
			{"_","_","+","+","+","O","+","+","+","_","_"},
			{"_","_","+","O","O","X","X","X","+","+","_"},
			{"_","+","+","+","+","+","+","+","+","+","_"},
			{"_","_","+","+","X","O","O","O","+","+","_"},
			{"_","_","+","+","+","X","+","+","+","_","_"},
			{"_","_","_","+","+","+","+","+","+","_","_"},
			{"_","_","_","+","+","+","+","+","_","_","_"},
			{"_","_","_","_","_","_","_","_","_","_","_"}
	};
	
	private String[][] board02 = {
			{"_","_","_","_","_","_","_","_","_","_","_"},
			{"_","_","_","+","+","+","+","+","_","_","_"},
			{"_","_","_","+","+","+","+","+","+","_","_"},
			{"_","_","+","+","+","+","+","+","X","_","_"},
			{"_","_","+","+","+","+","+","+","X","+","_"},
			{"_","+","+","+","+","+","+","+","+","+","_"},
			{"_","_","+","+","+","+","O","O","+","+","_"},
			{"_","_","+","+","+","+","+","+","+","_","_"},
			{"_","_","_","+","+","+","+","+","+","_","_"},
			{"_","_","_","+","+","+","+","+","_","_","_"},
			{"_","_","_","_","_","_","_","_","_","_","_"}
	};
	
	private String[][] board1 = {
			{"_","_","_","_","_","_","_","_","_","_","_"},
			{"_","_","_","X","X","X","X","X","_","_","_"},
			{"_","_","_","X","X","X","X","X","X","_","_"},
			{"_","_","+","+","X","X","X","+","+","_","_"},
			{"_","_","+","+","+","+","+","+","+","+","_"},
			{"_","+","+","+","+","+","+","+","+","+","_"},
			{"_","_","+","+","+","+","+","+","+","+","_"},
			{"_","_","+","+","O","O","O","+","+","_","_"},
			{"_","_","_","O","O","O","O","O","O","_","_"},
			{"_","_","_","O","O","O","O","O","_","_","_"},
			{"_","_","_","_","_","_","_","_","_","_","_"}
	};
	
	private int[][] board1Score = {
			{0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,5,10,10,10,5,0,0,0},
			{0,0,0,25,50,500,500,50,25,0,0},
			{0,0,0,0,100,250,100,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,100,250,100,0,0,0,0},
			{0,0,0,25,50,500,500,50,25,0,0},
			{0,0,0,5,10,10,10,5,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0},
	};
	
	private String[][] board2 = {
			{"_","_","_","_","_","_","_","_","_","_","_"},
			{"_","_","_","X","X","+","O","O","_","_","_"},
			{"_","_","_","X","X","X","O","O","O","_","_"},
			{"_","_","+","X","X","+","O","O","+","_","_"},
			{"_","_","+","+","+","+","+","+","+","+","_"},
			{"_","+","+","+","+","+","+","+","+","+","_"},
			{"_","_","+","+","+","+","+","+","+","+","_"},
			{"_","_","+","O","O","+","X","X","+","_","_"},
			{"_","_","_","O","O","O","X","X","X","_","_"},
			{"_","_","_","O","O","+","X","X","_","_","_"},
			{"_","_","_","_","_","_","_","_","_","_","_"}
	};
	
	private int[][] board2Score = {
			{0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,25,5,0,5,25,0,0,0},
			{0,0,0,50,500,10,10,500,50,0,0},
			{0,0,0,100,100,0,100,100,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0},
			{0,0,0,100,100,0,100,100,0,0,0},
			{0,0,0,50,500,10,10,500,50,0,0},
			{0,0,0,25,5,0,5,25,0,0,0},
			{0,0,0,0,0,0,0,0,0,0,0},
	};
	
	private ArrayList<Chip> chips;
	private ArrayList<Chip> selected;
	private ArrayList<Slot> availableLocation;
	private ArrayList<Chip> availablePushLocation;
	private ArrayList<Slot> slotList;
	private ArrayList<Chip.Neighbors> neighbors;
	private TweenManager manager;
	
	private Chip.Type turn;
	private ParticleDisplay particle;
	private String[][] board;
	private int[][] boardScore;
	private int removedWhiteChip;
	private int removedBlackChip;
	private int whiteScore;
	private int blackScore;
	private int movingObjects;
	private GameScreen screen;
	private TutorialScreen tutorialScreen;
	private int gameMode;
	
	private TextureRegion slotTexture;
	private TextureRegion blackTexture;
	private TextureRegion whiteTexture;
	private BitmapFont fontBlack;
	private BitmapFont fontWhite;
	
	private NinePatchDisplay dialogueWindow;
	public DisplayText closeText;
	private DisplayText noText;
	private DisplayText dialogueText;
	private DisplayText yesText;
	
	public GameBoard(int arr,int gameMode) {
		int rnd = (int) (Math.random() * 2);
		
		turn = (rnd == 0 ) ? Chip.Type.WHITE : Chip.Type.BLACK;
//		turn = Chip.Type.WHITE;
		
		ParticleEffect effect = Assets.getInstance().get("data/particle/destroy.p");
		particle = new ParticleDisplay(effect);
		addActor(particle);
		
		switch(arr) {
		case 0:
			board = board1;
			boardScore = board1Score;
			break;
		case 1:
			board = board2;
			boardScore = board2Score;
			break;
		}
		
		this.gameMode = gameMode;
		if(gameMode == PreGameScreen.GAME_MODE_TUTORIAL) {
			board = BoardArrangement.board1;
		}
//		board = board02;
		
		availableLocation = new ArrayList<Slot>();
		availablePushLocation = new ArrayList<Chip>();
		neighbors = new ArrayList<Chip.Neighbors>();
		selected = new ArrayList<Chip>();
		chips = new ArrayList<Chip>();
		slotList = new ArrayList<Slot>();
		
		createBackKeyCatcher();
	}
	
	private void createBackKeyCatcher() {
		BackKeyCatcher.getInstance().listener = new BackKeyListener() {
			
			@Override
			public void backPressed() {
				if(gameMode == PreGameScreen.GAME_MODE_TUTORIAL)  {
					screen.switchScreen(new MainMenuScreen());
				} else {
					showMessage("Are you sure you want to end the Game?",true);
				}
				
			}
		};
		
	}
	
	public void createNotification() {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		NinePatch ninePatch = atlas.createPatch("button");
		BitmapFont font = Assets.getInstance().generateFont(23, Assets.getInstance().getDefaultAllCharSet());
		BitmapFont cfont = Assets.getInstance().generateFont(18, Assets.getInstance().getDefaultAllCharSet());
		
		dialogueWindow = new NinePatchDisplay(ninePatch, 300, 150);
		dialogueWindow.setPosition(90,325);
		dialogueWindow.setAlpha(1f);
//		dialogueWindow.addListener(new ActorDragListener());
		screen.addActor(dialogueWindow);
		dialogueWindow.setVisible(false);
		
		closeText = new DisplayText("Close",cfont);
		closeText.setPreferredWidthInBounds(50);
//		closeText.addListener(new ActorDragListener());
		closeText.autoResize = false;
		closeText.setPosition(327,337);
		closeText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				dialogueWindow.setVisible(false);
				yesText.setVisible(false);
				closeText.setVisible(false);
				deselectAll();
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		closeText.setVisible(false);
		screen.addActor(closeText);
		
		noText = new DisplayText("No",font);
		noText.setPreferredWidthInBounds(50);
//		noText.addListener(new ActorDragListener());
		noText.autoResize = false;
		noText.setPosition(327,337);
		noText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				dialogueWindow.setVisible(false);
				yesText.setVisible(false);
				noText.setVisible(false);
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		noText.setVisible(false);
		screen.addActor(noText); 
		
		yesText = new DisplayText("Yes",font);
		yesText.setPreferredWidthInBounds(50);
		yesText.autoResize = false;
		yesText.setPosition(107,338);
//		yesText.addListener(new ActorDragListener());
		yesText.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				screen.switchScreen(new MainMenuScreen());
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		yesText.setVisible(false);
		screen.addActor(yesText);
		
		dialogueText = new DisplayText("",font);
		dialogueText.setAlignment(HAlignment.CENTER);
		dialogueText.setPreferedWidth(dialogueWindow.getWidth() - 20);
		dialogueWindow.addActor(dialogueText);
	}
	
	public void showMessage(String message,boolean end) {
		dialogueText.setText(message);
		dialogueText.setPosition(10, dialogueWindow.getHeight() - 30 - dialogueText.getTextHeight());
		
		dialogueWindow.setVisible(true);
		closeText.setVisible(true);
		
		if(end) {
			yesText.setVisible(true);
			closeText.setVisible(false);
			noText.setVisible(true);
		} else yesText.setVisible(false);
	}
	
	public void showMessage(String message,InputListener listener) {
		dialogueText.setText(message);
		dialogueText.setPosition(10, dialogueWindow.getHeight() - 30 - dialogueText.getTextHeight());
		
		dialogueWindow.setVisible(true);
		closeText.setVisible(true);
		closeText.addListener(listener);
		noText.setVisible(false);
		yesText.setVisible(false);
	}
	
	public void closeMessage() {
		dialogueWindow.setVisible(false);
		closeText.setVisible(false);
		yesText.setVisible(false);
		noText.setVisible(false);
	}
	
	public void deselectAll() {
		for(Chip chip:selected){
			chip.isSelected = false;
		}
		removeListeners(availableLocation);
		removeChipPushListeners(availablePushLocation);
		availableLocation.clear();
		availablePushLocation.clear();
		neighbors.clear();
		selected.clear();
	}
	
	public void init() {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		slotTexture = atlas.findRegion("empty");
		blackTexture = atlas.findRegion("black");
		whiteTexture = atlas.findRegion("white");
		
		fontWhite = Assets.getInstance().generateFont(20, Assets.getInstance().getDefaultAllCharSet());
		fontBlack = Assets.getInstance().generateFont(20, Assets.getInstance().getDefaultAllCharSet());
		BitmapFont scoreFont = Assets.getInstance().generateFont(30, Assets.getInstance().getDefaultAllCharSet());
		fontBlack.setColor(Color.BLACK);
		
		final Chip blackChip = new Chip(blackTexture, Chip.Type.BLACK,fontWhite, 0);
		blackChip.setPosition(273,124);
		if(gameMode != PreGameScreen.GAME_MODE_TUTORIAL)
			addActor(blackChip);
		blackChip.addAction(new Action() {
			
			@Override
			public boolean act(float delta) {
				blackChip.getTextDisplay().setText(""+removedWhiteChip);
				return false;
			}
		});
		
		final Chip whiteChip = new Chip(whiteTexture, Chip.Type.WHITE,fontBlack, 0);
		whiteChip.setPosition(279,-624);
		if(gameMode != PreGameScreen.GAME_MODE_TUTORIAL)
			addActor(whiteChip);
		whiteChip.addAction(new Action() {
			
			@Override
			public boolean act(float delta) {
				whiteChip.getTextDisplay().setText(""+removedBlackChip);
				return false;
			}
		});
		
		final DisplayText blackScoreDisplay = new DisplayText("SCORE\n",scoreFont);
		blackScoreDisplay.autoResize = false;
		blackScoreDisplay.setPreferedWidth(200);
		blackScoreDisplay.setAlignment(HAlignment.CENTER);
		blackScoreDisplay.setPosition(200, 40);
		if(gameMode != PreGameScreen.GAME_MODE_TUTORIAL)
			addActor(blackScoreDisplay);
		blackScoreDisplay.addAction(new Action() {
			float scoreCounter = 0;
			float difference = 0;
			@Override
			public boolean act(float delta) {
				float dps = 1f / delta;
				if(difference == 0) difference = blackScore - scoreCounter;
				
				float sps =  difference / dps;
				scoreCounter += sps;
				if(scoreCounter >= blackScore) {
					scoreCounter = blackScore;
					difference = 0;
				}
				blackScoreDisplay.setText("SCORE\n"+(int)(scoreCounter));
				return false;
			}
		});
		
		final DisplayText whiteScoreDisplay = new DisplayText("SCORE\n",scoreFont);
		whiteScoreDisplay.autoResize = false;
		whiteScoreDisplay.setPreferedWidth(200);
		whiteScoreDisplay.setAlignment(HAlignment.CENTER);
		whiteScoreDisplay.setPosition(200, -555);
		if(gameMode != PreGameScreen.GAME_MODE_TUTORIAL)
			addActor(whiteScoreDisplay);
		whiteScoreDisplay.addAction(new Action() {
			float scoreCounter = 0;
			float difference = 0;
			@Override
			public boolean act(float delta) {
				float dps = 1f / delta;
				if(difference == 0) difference = whiteScore - scoreCounter;
				
				float sps =  difference / dps;
				scoreCounter += sps;
				if(scoreCounter >= whiteScore) {
					scoreCounter = whiteScore;
					difference = 0;
				}
				whiteScoreDisplay.setText("SCORE\n"+(int)(scoreCounter));
				return false;
			}
		});
		
		designBoard();
		
	}
	
	public void clearAllActors() {
		for(int i=slotList.size()-1;i>=0;i-- ) {
			Slot slot = slotList.get(i);
			slot.remove();
		}
		
		for(int i=chips.size()-1;i>=0;i-- ) {
			Chip chip = chips.get(i);
			chip.remove();
		}
	}
	
	public void designBoard() {
		availableLocation.clear();
		availablePushLocation.clear();
		neighbors.clear();
		selected.clear();
		
		clearAllActors();
		
		chips.clear();
		slotList.clear();
		
		//create board with empty slots
		slots = new Slot[BOARD_MAX_ROW][BOARD_MAX_COLUMN];
		for(int row=0;row<BOARD_MAX_ROW;row++) {
			for(int col=0;col<BOARD_MAX_COLUMN;col++) {
				Slot slot = new Slot(slotTexture, row, col);
//				slot.isDebug = true;
				slot.setOrigin(slot.getWidth()/2, slot.getHeight()/2);
				float posX = (col * (slotTexture.getRegionWidth())) + ((row % 2) * (slotTexture.getRegionWidth()/2));
				float posY =  - (row * slotTexture.getRegionHeight());
				slot.setPosition(posX, posY);
				
//				DisplayText rowAndFont = new DisplayText(row+"/"+col, fontWhite);
//				slot.addActor(rowAndFont);
				
				if(board[row][col].equalsIgnoreCase("_")){
					slot.setVisible(false);
				}
				
				slots[row][col] = slot;
				addActor(slot);
				slotList.add(slot);
			}
		}
		
		//fill board with black and white chips
		for(int row=0;row<BOARD_MAX_ROW;row++) {
			for(int col=0;col<BOARD_MAX_COLUMN;col++) {
				Slot slot = slots[row][col];
				Chip.Type type = null;
				Color textColor = null;
				BitmapFont font = null;
				int number = boardScore[row][col];
				TextureRegion chipTexture = null;
				
				if(board[row][col].equalsIgnoreCase("X")) {
					chipTexture = blackTexture;
					type = Chip.Type.BLACK;
					font = fontWhite;
				} else if(board[row][col].equalsIgnoreCase("O")) {
					chipTexture = whiteTexture;
					type = Chip.Type.WHITE;
					font = fontBlack;
				}
				if(type == null) continue;
				
				Chip chip = new Chip(chipTexture, type,font, number);
				chip.setPosition(slot.getX(), slot.getY());
				chip.setLocation(row, col);
				chip.setOrigin(chip.getWidth()/2, chip.getHeight()/2);
				chip.setTweenManager(manager);
				chips.add(chip);
				addActor(chip);
				
				if(gameMode == PreGameScreen.GAME_MODE_TUTORIAL){
					chip.getTextDisplay().setVisible(false);
				}
			}
		}
		//add listener to chips
		final GameBoard board = this;
		for(int i=0;i < chips.size();i++){
			final Chip chip = chips.get(i);
			chip.addListener(new ChipListener(){
				@Override
				public void onTap() {
					if(movingObjects > 0) {
						return;
					}
					
					if(turn == Chip.Type.BLACK)
						if(gameMode == PreGameScreen.GAME_MODE_PLAYER_VS_AI)
							return;
					
					if(selected.size() == 0) {
						if(chip.getType() == turn) {
							
							selected.add(chip);
							neighbors.add(Chip.Neighbors.EAST);
							neighbors.add(Chip.Neighbors.WEST);
							neighbors.add(Chip.Neighbors.NORTH_EAST);
							neighbors.add(Chip.Neighbors.NORTH_WEST);
							neighbors.add(Chip.Neighbors.SOUTH_EAST);
							neighbors.add(Chip.Neighbors.SOUTH_WEST);
							chip.tap(Tap.SELECTED);
							if(gameMode!=PreGameScreen.GAME_MODE_TUTORIAL) {
								dialogueWindow.setVisible(false);
								closeText.setVisible(false);
								yesText.setVisible(false);
							}
							availableLocation = getAvailableLocation(chip);
							for(int j = 0;j<availableLocation.size();j++) {
								final Slot slot = availableLocation.get(j);
								if(slot.isVisible()) {
									slot.setAvailable(true);
									slot.setChipListener(new ChipListener(){
										public void onTap() {
											movingObjects += 1;
											chip.moveTween(slot,board);
											removeListeners(availableLocation);
											chip.isSelected = false;
											selected.clear();
											neighbors.clear();
											Assets.getInstance().playSound("sfx/movechip.mp3");
										};
									});
								}
								
							}
						} else {
							if(gameMode == PreGameScreen.GAME_MODE_TUTORIAL) return;
							showMessage("It is not your chip!", false);
							noText.setVisible(false);
							L.e("not your turn!");
						}
					} else if(selected.size() > 0) {
						if(chip.getType() == turn) {
							
							if(selected.contains(chip)) {
								for(Chip chip:selected){
									chip.isSelected = false;
								}
								removeListeners(availableLocation);
								removeChipPushListeners(availablePushLocation);
								
								neighbors.clear();
								selected.clear();
								return;
							}
							if(selected.size() == 3) return;
							
							if(isNeigbor(chip)) {
								selected.add(chip);
								
								chip.tap(Tap.SELECTED);
								if(gameMode != PreGameScreen.GAME_MODE_TUTORIAL) {
									dialogueWindow.setVisible(false);
									closeText.setVisible(false);
									yesText.setVisible(false);
								}
								
								removeListeners(availableLocation);
								availableLocation.clear();
								
								availableLocation = getAvailableSlotLocation(selected);
								ArrayList<Slot> availableLeapLocation = getAvailableLeapLocation(selected);
								availableLocation.addAll(availableLeapLocation);
								for(int j = 0;j<availableLocation.size();j++) {
									final Slot slot = availableLocation.get(j);
									if(slot.isVisible()) {
										slot.setAvailable(true);
										slot.setChipListener(new ChipListener(){
											public void onTap() {
												moveSelected(slot,selected);
												
												removeListeners(availableLocation);
												removeChipPushListeners(availablePushLocation);
												
												neighbors.clear();
												selected.clear();
												
											};
										});
									}
								}
								
								availablePushLocation = getAvailablePushLocation(selected);
								for(int k=0;k<availablePushLocation.size();k++) {
									final Chip chipToPush = availablePushLocation.get(k);
									chipToPush.isAvailable = true;
									chipToPush.setPushListener(new ChipListener(){
										public void onTap() {
											Slot slotTraget = slots[chipToPush.getRow()][chipToPush.getColumn()];
											//L.wtf("selected size is "+selected.size());
											Neighbors direction = moveSelected(slotTraget,selected);
											//L.wtf("selected size is "+selected.size());
											pushNeighbors(chipToPush,direction);
											
											removeListeners(availableLocation);
											removeChipPushListeners(availablePushLocation);
											
											neighbors.clear();
											selected.clear();
											
										};
									});
								}
								
								
							} else {
								if(gameMode == PreGameScreen.GAME_MODE_TUTORIAL) return;
								showMessage(turn.toString()+" selection invalid!",false);
								noText.setVisible(false);
								L.wtf("not neighbor");
							}
						}
						
					}
					super.onTap();
				}
			});
		}
		
	}
	
	public Neighbors moveSelected(Slot slot,ArrayList<Chip> selected){
		Neighbors destination = null;
		
		Chip northWestChip = selected.get(0);
		Chip northEastChip = selected.get(0);
		Chip southWestChip = selected.get(0);
		Chip southEastChip = selected.get(0);
		
		for(int i=0;i<selected.size();i++) {
			Chip chip = selected.get(i);
			if(chip.getRow() < northWestChip.getRow() ) {
				
					northWestChip = chip;
			} else if(chip.getRow() == northWestChip.getRow()) {
				if(chip.getColumn() < northWestChip.getColumn())
					northWestChip = chip;
			}
			
			if(chip.getRow() < northEastChip.getRow() ) {
					northEastChip = chip;
			} else if(chip.getRow() == northEastChip.getRow()) {
				L.wtf("chip r:"+chip.getRow()+" c:"+chip.getColumn());
				L.wtf("northEastChip r:"+northEastChip.getRow()+" c:"+northEastChip.getColumn());
				if(chip.getColumn() > northEastChip.getColumn()) {
					L.wtf("east");
					northEastChip = chip;
				}
			}
			
			if(chip.getRow() > southWestChip.getRow()) {
					southWestChip = chip;
			} else if(chip.getRow() == southWestChip.getRow()) {
				if(chip.getColumn() < southWestChip.getColumn())
					southWestChip = chip;
			}
			
			if(chip.getRow() > southEastChip.getRow()) {
				southEastChip = chip;
			} else if(chip.getRow() == southEastChip.getRow()) {
				if(chip.getColumn() > southEastChip.getColumn())
					southEastChip = chip;
			}
			
		}
		
		//east or west
		if(slot.getRow() == northEastChip.getRow() || slot.getRow() == southWestChip.getRow()){
			L.wtf("1");
			//west
			if(slot.getColumn() < southWestChip.getColumn()) {
				destination = Neighbors.WEST;
			}
			//east
			else if(slot.getColumn() > northEastChip.getColumn()) {
				destination = Neighbors.EAST;
			}
		}
		//north east or north west
		if(slot.getRow() < northEastChip.getRow()) {
			L.wtf("2");
			//north west
			if(slot.getColumn() == northWestChip.getUpperLeftColumn()) {
				destination = Neighbors.NORTH_WEST;
			}
			//north east
			else if(slot.getColumn() == northEastChip.getUpperRightColumn()) {
				destination = Neighbors.NORTH_EAST;
			}
		}
		//south east or south west
		if(slot.getRow() > southEastChip.getRow() ) {
			L.wtf("3");
			//south west
			if(slot.getColumn() == southWestChip.getLowerLeftColumn()) {
				destination = Neighbors.SOUTH_WEST;
			}
			//south east
			else if(slot.getColumn() == southEastChip.getLowerRightColumn()) {
				destination = Neighbors.SOUTH_EAST;
			}
		}
		
		if(destination == null){
			L.wtf("slot r:"+slot.getRow()+" c:"+slot.getColumn());
			L.wtf("---------------");
			for(int i=0;i<selected.size();i++){
				Chip c = selected.get(i);
				L.wtf("chip r:"+c.getRow()+" c:"+c.getColumn());
			}
		}
		
		for(int i=0;i<selected.size();i++) {
			Chip chip = selected.get(i);
			int row = 0;
			int col = 0;
			
			switch (destination) {
			case EAST:
				row = chip.getRow();
				col = chip.getColumn() + 1;
				break;
			case WEST:
				row = chip.getRow();
				col = chip.getColumn() - 1;
				break;
			case NORTH_EAST:
				row = chip.getRow() - 1;
				col = chip.getUpperRightColumn();
				break;
			case NORTH_WEST:
				row = chip.getRow() - 1;
				col = chip.getUpperLeftColumn();
				break;
			case SOUTH_EAST:
				row = chip.getRow() + 1;
				col = chip.getLowerRightColumn();
				break;
			case SOUTH_WEST:
				row = chip.getRow() + 1;
				col = chip.getLowerLeftColumn();
				break;
			default:
				break;
			}
			
			Slot targetSlot = slots[row][col];
			chip.moveTween(targetSlot,this);
			chip.isSelected = false;
			movingObjects += 1;
		}
		
		Assets.getInstance().playSound("sfx/movechip.mp3");
		
		return destination;
	}
	
	public void pushNeighbors(Chip chip,Neighbors neighbor) {
		neighbors.clear();
		neighbors.add(neighbor);
		ArrayList<Chip> slantNeighbors = getSlantNeighborChips(chip);
		ArrayList<Slot> availableSlotLocation = getAvailableSlotLocation(slantNeighbors);
		Slot availableSlot = availableSlotLocation.get(0);
		moveSelected(availableSlot, slantNeighbors);
	}
	
	private boolean isNeigbor(Chip chip) {
		
		for(int i=0;i<selected.size();i++){
			Chip selectedChip = selected.get(i);
			if(availableNeighbor(Chip.Neighbors.NORTH_EAST)) {
				int upperRight = selectedChip.getUpperRightColumn();
				int row = selectedChip.getRow();
				if(row-1 >= 0){
					Slot slot = slots[row-1][upperRight];
					if(chip.getRow()==slot.getRow() && chip.getColumn()==slot.getColumn()){
						neighbors.clear();
						neighbors.add(Chip.Neighbors.NORTH_EAST);
						neighbors.add(Chip.Neighbors.SOUTH_WEST);
						return true;
					}
				}
			}
			if(availableNeighbor(Chip.Neighbors.NORTH_WEST)) {
				int upperLeft = selectedChip.getUpperLeftColumn();
				int row = selectedChip.getRow();
				if(row - 1 >= 0) {
					Slot slot = slots[row-1][upperLeft];
					if(chip.getRow()==slot.getRow() && chip.getColumn()==slot.getColumn()){
						neighbors.clear();
						neighbors.add(Chip.Neighbors.NORTH_WEST);
						neighbors.add(Chip.Neighbors.SOUTH_EAST);
						return true;
					}
				}
			}
			if(availableNeighbor(Chip.Neighbors.SOUTH_EAST)) {
				int lowerRight = selectedChip.getLowerRightColumn();
				int row = selectedChip.getRow();
				if(row+1 < BOARD_MAX_ROW) {
					Slot slot = slots[row+1][lowerRight];
					if(chip.getRow()==slot.getRow() && chip.getColumn()==slot.getColumn()){
						neighbors.clear();
						neighbors.add(Chip.Neighbors.SOUTH_EAST);
						neighbors.add(Chip.Neighbors.NORTH_WEST);
						return true;
					}
				}
			}
			if(availableNeighbor(Chip.Neighbors.SOUTH_WEST)) {
				int lowerLeft = selectedChip.getLowerLeftColumn();
				int row = selectedChip.getRow();
				if(row+1 < BOARD_MAX_ROW) {
					Slot slot = slots[row+1][lowerLeft];
					if(chip.getRow()==slot.getRow() && chip.getColumn()==slot.getColumn()) {
						neighbors.clear();
						neighbors.add(Chip.Neighbors.NORTH_EAST);
						neighbors.add(Chip.Neighbors.SOUTH_WEST);
						return true;
					}
				}
			}
			if(availableNeighbor(Chip.Neighbors.EAST)) {
				int right = selectedChip.getColumn() + 1;
				int row = selectedChip.getRow();
				if(right < BOARD_MAX_COLUMN) {
					Slot slot = slots[row][right];
					if(chip.getRow()==slot.getRow() && chip.getColumn()==slot.getColumn()) {
						neighbors.clear();
						neighbors.add(Chip.Neighbors.EAST);
						neighbors.add(Chip.Neighbors.WEST);
						return true;
					}
				}
			}
			if(availableNeighbor(Chip.Neighbors.WEST)) {
				int left = selectedChip.getColumn() - 1;
				int row = selectedChip.getRow();
				if(left >= 0) {
					Slot slot = slots[row][left];
					if(chip.getRow()==slot.getRow() && chip.getColumn()==slot.getColumn()) {
						neighbors.clear();
						neighbors.add(Chip.Neighbors.EAST);
						neighbors.add(Chip.Neighbors.WEST);
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private boolean availableNeighbor(Chip.Neighbors neighbor) {
		for(int i=0;i<neighbors.size();i++) {
			if(neighbor.equals(neighbors.get(i))) {
				return true;
			}
		}
		
		return false;
	}
	
	public boolean availableNeighbor(Chip.Neighbors neighbor,ArrayList<Chip.Neighbors> list) {
		for(int i=0;i<list.size();i++) {
			if(neighbor.equals(list.get(i))) {
				return true;
			}
		}
		
		return false;
	}
	
	public void removeListeners(ArrayList<Slot> list) {
		for(Slot slot: list) {
			slot.setAvailable(false);
			slot.removeChipListener();
		}
	}
	
	public void removeChipPushListeners(ArrayList<Chip> list) {
		for(Chip chip: list) {
			chip.isAvailable = false;
			chip.removePushListener();
		}
	}

	public void setTweenManager(TweenManager manager) {
		this.manager = manager;
	}
	
	public ArrayList<Slot> getAvailableSlotLocation(ArrayList<Chip> selected) {
		ArrayList<Slot> availableSlots = new ArrayList<Slot>();
		ArrayList<Slot> availableSlantLocation = getAvailableSlantLocation(selected);
		for(Slot slot:availableSlantLocation) {
			if(getChip(slot.getRow(), slot.getColumn())==null) {
				availableSlots.add(slot);
			}
		}
		return availableSlots;
	}
	
	public ArrayList<Chip> getAvailablePushLocation(ArrayList<Chip> selected) {
		ArrayList<Chip> availablePush = new ArrayList<Chip>();
		ArrayList<Slot> availableSlantLocation = getAvailableSlantLocation(selected);
		for(Slot slot:availableSlantLocation) {
			Chip chip = getChip(slot.getRow(), slot.getColumn());
			if(chip != null && chip.getType() != turn) {
				
				ArrayList<Chip> slantNeighbors = getSlantNeighborChips(chip);
//				L.wtf("col = "+slantNeighbors.get(0).getColumn());
				ArrayList<Slot> availableSlotLocation = getAvailableSlotLocation(slantNeighbors);
				if(slantNeighbors.size() < selected.size() && availableSlotLocation.size() >= 1) {
					availablePush.add(chip);
				}
			}
		}
		return availablePush;
	}
	
	public ArrayList<Slot> getAvailableLeapLocation(ArrayList<Chip> selected) {
		ArrayList<Slot> availableLeapLocation = new ArrayList<Slot>();
		
		Chip northWestChip = selected.get(0);
		Chip northEastChip = selected.get(0);
		Chip southWestChip = selected.get(0);
		Chip southEastChip = selected.get(0);
		
		boolean moveEast = true;
		boolean moveWest = true;
		boolean moveNorthEast = true;
		boolean moveNothWest = true;
		boolean moveSouthEast = true;
		boolean moveSouthWest = true;
		
		for(int i=0;i<selected.size();i++) {
			Chip chip = selected.get(i);
			if(chip.getRow() < northWestChip.getRow() ) {
				
					northWestChip = chip;
			} else if(chip.getRow() == northWestChip.getRow()) {
				if(chip.getColumn() < northWestChip.getColumn())
					northWestChip = chip;
			}
			
			if(chip.getRow() < northEastChip.getRow() ) {
					northEastChip = chip;
			} else if(chip.getRow() == northEastChip.getRow()) {
				if(chip.getColumn() > northEastChip.getColumn())
					northEastChip = chip;
			}
			
			if(chip.getRow() > southWestChip.getRow()) {
					southWestChip = chip;
			} else if(chip.getRow() == southWestChip.getRow()) {
				if(chip.getColumn() < southWestChip.getColumn())
					southWestChip = chip;
			}
			
			if(chip.getRow() > southEastChip.getRow()) {
				southEastChip = chip;
			} else if(chip.getRow() == southEastChip.getRow()) {
				if(chip.getColumn() > southEastChip.getColumn())
					southEastChip = chip;
			}
			
			if(moveEast) {
				if(!slotAvailable(Neighbors.EAST , chip)) moveEast = false;
			}
			if(moveWest) {
				if(!slotAvailable(Neighbors.WEST , chip)) moveWest = false;
			}
			if(moveNorthEast) {
				if(!slotAvailable(Neighbors.NORTH_EAST , chip)) moveNorthEast = false;
			}
			if(moveNothWest) {
				if(!slotAvailable(Neighbors.NORTH_WEST , chip)) moveNothWest = false;
			}
			if(moveSouthEast) {
				if(!slotAvailable(Neighbors.SOUTH_EAST , chip)) moveSouthEast = false;
			}
			if(moveSouthWest) {
				if(!slotAvailable(Neighbors.SOUTH_WEST , chip)) moveSouthWest = false;
			}
			
		}
		if(moveEast) {
			Slot slot = slots[northEastChip.getRow()][northEastChip.getColumn() + 1];
			if(slot.isVisible())
				availableLeapLocation.add(slot);
		}
		if(moveWest) {
			Slot slot = slots[southWestChip.getRow()][southWestChip.getColumn() - 1];
			if(slot.isVisible())
				availableLeapLocation.add(slot);
		}
		if(moveNorthEast) {
			Slot slot = slots[northEastChip.getRow()-1][northEastChip.getUpperRightColumn()];
			if(slot.isVisible())
				availableLeapLocation.add(slot);
		}
		if(moveNothWest) {
			Slot slot = slots[northWestChip.getRow()-1][northWestChip.getUpperLeftColumn()];
			if(slot.isVisible())
				availableLeapLocation.add(slot);
		}
		if(moveSouthEast) {
			Slot slot = slots[southEastChip.getRow()+1][southEastChip.getLowerRightColumn()];
			if(slot.isVisible())
				availableLeapLocation.add(slot);
		}
		if(moveSouthWest) {
			Slot slot = slots[southWestChip.getRow()+1][southWestChip.getLowerLeftColumn()];
			if(slot.isVisible())
				availableLeapLocation.add(slot);
		}
		
		return availableLeapLocation;
	}
	
	public boolean slotAvailable(Neighbors neighbor,Chip chip) {
		Chip c = null;
		Slot slot = null;
		switch (neighbor) {
		case EAST:
			c = getChip(chip.getRow(), chip.getColumn() + 1);
			slot = slots[chip.getRow()][chip.getColumn() + 1];
			break;
		case WEST:
			c = getChip(chip.getRow(), chip.getColumn() - 1);
			slot = slots[chip.getRow()][chip.getColumn() - 1];
			break;
		case NORTH_EAST:
			c = getChip(chip.getRow()-1, chip.getUpperRightColumn());
			slot = slots[chip.getRow()-1][chip.getUpperRightColumn()];
			break;
		case NORTH_WEST:
			c = getChip(chip.getRow()-1, chip.getUpperLeftColumn());
			slot = slots[chip.getRow()-1][chip.getUpperLeftColumn()];
			break;
		case SOUTH_EAST:
			c = getChip(chip.getRow()+1, chip.getLowerRightColumn());
			slot = slots[chip.getRow()+1][chip.getLowerRightColumn()];
			break;
		case SOUTH_WEST:
			c = getChip(chip.getRow()+1, chip.getLowerLeftColumn());
			slot = slots[chip.getRow()+1][chip.getLowerLeftColumn()];
			break;
		default:
			break;
		}
		
		if(c == null && slot != null) {
			if(slot.isVisible()) return true;
		}
			
		return false;
	}
	
	public Chip getNeighborChip(Neighbors neighbor,Chip chip) {
		Chip neighborChip = null;
		switch (neighbor) {
		case EAST:
			neighborChip = getChip(chip.getRow(), chip.getColumn() + 1);
			break;
		case WEST:
			neighborChip = getChip(chip.getRow(), chip.getColumn() - 1);
			break;
		case NORTH_EAST:
			neighborChip = getChip(chip.getRow()-1, chip.getUpperRightColumn());
			break;
		case NORTH_WEST:
			neighborChip = getChip(chip.getRow()-1, chip.getUpperLeftColumn());
			break;
		case SOUTH_EAST:
			neighborChip = getChip(chip.getRow()+1, chip.getLowerRightColumn());
			break;
		case SOUTH_WEST:
			neighborChip = getChip(chip.getRow()+1, chip.getLowerLeftColumn());
			break;
		default:
			break;
		}
		
		return neighborChip;
	}
	
	public ArrayList<Chip> getSlantNeighborChips(Chip chip) {
		return getSlantNeighborChips(chip, 3);
		
	}
	
	public ArrayList<Chip> getSlantNeighborChips(Chip chip , int max) {
		ArrayList<Chip> slantNeighborChips = new ArrayList<Chip>();
		ArrayList<Chip.Neighbors> neighborList = new ArrayList<Chip.Neighbors>();
		neighborList.addAll(neighbors);
		
		Chip selectedChip = selected.get(0);
		int rowIterator = 0;
		int col = 0;
		int colIterator = 0;
		int startingRow = chip.getRow();
		
		Chip chipTemp = chip;
		while(chipTemp!=null) {
			
			slantNeighborChips.add(chipTemp);
			if(slantNeighborChips.size() >= max) {
				break;
			}
			if(chip.getRow() < selectedChip.getRow()) {
				rowIterator += -1;
				if(availableNeighbor(Chip.Neighbors.NORTH_WEST,neighborList)) {
					col = chipTemp.getUpperLeftColumn();
					neighborList.clear();
					neighborList.add(Neighbors.NORTH_WEST);
				} else if(availableNeighbor(Chip.Neighbors.NORTH_EAST,neighborList)) {
					col = chipTemp.getUpperRightColumn();
					neighborList.clear();
					neighborList.add(Neighbors.NORTH_EAST);
				}
			} else if(chip.getRow() > selectedChip.getRow()) {
				rowIterator += 1;
				if(availableNeighbor(Chip.Neighbors.SOUTH_WEST,neighborList)) {
					col = chipTemp.getLowerLeftColumn();
					neighborList.clear();
					neighborList.add(Neighbors.SOUTH_WEST);
				} else if(availableNeighbor(Chip.Neighbors.SOUTH_EAST,neighborList)){
					col = chipTemp.getLowerRightColumn();
					neighborList.clear();
					neighborList.add(Neighbors.SOUTH_EAST);
				}
			} else if(chip.getRow() == selectedChip.getRow() ) {
				rowIterator = 0;
				col = chip.getColumn();
				if(chip.getColumn() < selectedChip.getColumn()) {
					colIterator += -1;
					neighborList.clear();
					neighborList.add(Neighbors.WEST);
				} else if(chip.getColumn() > selectedChip.getColumn()) {
					colIterator += 1;
					neighborList.clear();
					neighborList.add(Neighbors.EAST);
				}
				
			}
			Chip.Type lastType = chipTemp.getType();
			chipTemp = getChip(startingRow + rowIterator, col + colIterator);
			
			if(chipTemp!=null){
				if(chipTemp.getType() != lastType) break;
			}
		}
		
		return slantNeighborChips;
	}
	
	private ArrayList<Slot> getAvailableSlantLocation(ArrayList<Chip> selected) {
		ArrayList<Slot> availableSlantLocation = new ArrayList<Slot>();
		Chip lowestRow = selected.get(0);
		Chip highestRow = selected.get(0);
		Chip lowestColumn = selected.get(0);
		Chip highestColumn = selected.get(0);
		
		for(int i=0;i<selected.size();i++) {
			Chip chip = selected.get(i);
			if(lowestRow.getRow() > chip.getRow()) {
				lowestRow = chip;
			}
			if(highestRow.getRow() < chip.getRow()) {
				highestRow = chip;
			}
			if(lowestColumn.getColumn() > chip.getColumn()) {
				lowestColumn = chip;
			}
			if(highestColumn.getColumn() < chip.getColumn()) {
				highestColumn = chip;
			}
		}
		
		int lowestRowLocation = lowestRow.getRow()-1;
		int highestRowLocation = highestRow.getRow()+1;
		if(lowestRowLocation >= 0) {
			if(availableNeighbor(Chip.Neighbors.NORTH_EAST)) {
				Slot slot = slots[lowestRowLocation][lowestRow.getUpperRightColumn()];
				//if(slot.isVisible())
					availableSlantLocation.add(slot);
			} else if(availableNeighbor(Chip.Neighbors.NORTH_WEST)) {
				Slot slot = slots[lowestRowLocation][lowestRow.getUpperLeftColumn()];
				//if(slot.isVisible())
					availableSlantLocation.add(slot);
			}
		}
		if(highestRowLocation < BOARD_MAX_ROW){
			if(availableNeighbor(Neighbors.SOUTH_EAST)) {
				Slot slot = slots[highestRowLocation][highestRow.getLowerRightColumn()];
				//if(slot.isVisible())
					availableSlantLocation.add(slot);
			} else if(availableNeighbor(Chip.Neighbors.SOUTH_WEST)) {
				Slot slot = slots[highestRowLocation][highestRow.getLowerLeftColumn()];
				//if(slot.isVisible())
					availableSlantLocation.add(slot);
			}
		}
		if(lowestColumn.getColumn() - 1 >= 0) {
			if(availableNeighbor(Neighbors.WEST)) {
				Slot slot = slots[lowestRow.getRow()][lowestColumn.getColumn() - 1];
				//if(slot.isVisible())
					availableSlantLocation.add(slot);
			}
		}
		if(highestColumn.getColumn() + 1 < BOARD_MAX_COLUMN) {
			if(availableNeighbor(Neighbors.EAST)) {
				Slot slot = slots[lowestRow.getRow()][highestColumn.getColumn() + 1];
				//if(slot.isVisible())
					availableSlantLocation.add(slot);
			}
		}
		
		return availableSlantLocation;
	}
	
	public ArrayList<Slot> getAvailableLocation(Chip chip) {
		int upperLeft = chip.getUpperLeftColumn();
		int upperRight = chip.getUpperRightColumn();
		int lowerLeft = chip.getLowerLeftColumn();
		int lowerRight = chip.getLowerRightColumn();
		int row = chip.getRow();
		int col = chip.getColumn();
		
		ArrayList<Slot> availableLocation = new ArrayList<Slot>();
		
//		L.wtf("row "+row);
//		L.wtf("col "+col);
//		L.wtf("upperLeft "+upperLeft);
		//L.wtf("col "+col);
		
		if(upperLeft >= 0 && row - 1 >= 0){
			Chip upperLeftChip = getChip(row - 1, upperLeft);
			Slot slot = slots[row-1][upperLeft];
			if(slot.isVisible()) {
				if(upperLeftChip == null && slot.isVisible()){
					availableLocation.add(slots[row-1][upperLeft]);
				} else {
	//				L.wtf("not null r="+upperLeftChip.getRow()+" c="+upperLeftChip.getColumn());
				}
			}
		}
		if(upperRight < BOARD_MAX_COLUMN && row - 1 >= 0) {
			Chip upperRightChip = getChip(row - 1, upperRight);
			Slot slot = slots[row-1][upperRight];
			if(slot.isVisible()) {
				if(upperRightChip == null && slot.isVisible()) {
					availableLocation.add(slots[row-1][upperRight]);
				}
			}
		}
		if(lowerLeft >= 0 && row + 1 < BOARD_MAX_ROW) {
			Chip lowerLeftChip = getChip(row + 1, lowerLeft);
			Slot slot = slots[row+1][lowerLeft];
			if(slot.isVisible()) {
				if(lowerLeftChip == null && slot.isVisible()) {
					availableLocation.add(slots[row+1][lowerLeft]);
				}
			}
		}
		if(lowerRight < BOARD_MAX_COLUMN && row + 1 < BOARD_MAX_ROW) {
			Chip lowerRighChip = getChip(row + 1, lowerRight);
			Slot slot = slots[row+1][lowerRight];
			if(slot.isVisible()) {
				if(lowerRighChip == null && slot.isVisible()) {
					availableLocation.add(slots[row+1][lowerRight]);
				}
			}
		}
		if(col - 1 >= 0) {
			Chip leftChip = getChip(row, col-1);
			Slot slot = slots[row][col-1];
			if(slot.isVisible()) {
				if(leftChip == null && slot.isVisible()) {
					availableLocation.add(slots[row][col -1]);
				}
			}
		}
		if(col + 1 < BOARD_MAX_COLUMN) {
			Chip rightChip = getChip(row, col+1);
			Slot slot = slots[row][col+1];
			if(slot.isVisible()) {
				if(rightChip == null && slot.isVisible()) {
					availableLocation.add(slots[row][col+1]);
				}
			}
		}
		
		return availableLocation;
	}
	
	public Chip getChip(int row,int col) {
		for(Chip chip:chips){
			if(chip.getRow() == row && chip.getColumn() == col)
				return chip;
		}
		return null;
	}
	
	public void changedTurn(){
		clearObjects();
		if(gameMode == PreGameScreen.GAME_MODE_TUTORIAL)
			return;
			
		turn = (turn == Chip.Type.WHITE) ? Chip.Type.BLACK : Chip.Type.WHITE;
		//L.wtf(turn.toString()+" TURN!!!");
		
		
		screen.startTurn();
	}
	
	@Override
	public void done() {
		movingObjects -= 1;
		//L.wtf("moving objects "+movingObjects);
		removeOutChips();
		if(movingObjects == 0) {
			changedTurn();
		}
	}
	
	private void clearObjects() {
		removeListeners(availableLocation);
		availableLocation.clear();
		availablePushLocation.clear();
		neighbors.clear();
		selected.clear();
	}
	
	private void removeOutChips() {
		for(int i=0;i<chips.size();i++) {
			final Chip chip = chips.get(i);
			if(chip.getRow() == 0 || chip.getColumn() == 0 || chip.getRow() == BOARD_MAX_ROW - 1 || chip.getColumn() == BOARD_MAX_COLUMN - 1 || deadSlot(chip)) {
				if(chip.getType() == Chip.Type.WHITE) {
					removedWhiteChip += 1;
					blackScore += chip.getNumber();
				} else {
					removedBlackChip += 1;
					whiteScore += chip.getNumber();
				}
				movingObjects += 1;
				chip.outTween(new MoveCallback() {
					
					@Override
					public void done() {
						movingObjects -= 1;
						Assets.getInstance().playSound("sfx/lostchip.mp3");
						particle.reset();
						particle.setPosition(chip.getX()+chip.getOriginX(), chip.getY()+chip.getOriginY());
						particle.setCompleteListener(new ParticleDisplayListener() {
							
							@Override
							public void complete() {
								checkIfSomeoneWon();
								if(movingObjects == 0) {
									changedTurn();
								}
							}
						});
					}
				});
				chips.remove(chip);
				break;
			}
		}
	}
	
	private boolean deadSlot(Chip chip) {
		Slot slot = slots[chip.getRow()][chip.getColumn()];
		if(!slot.isVisible()) return true;
		else return false;
	}
	
	public Chip.Type getTurn () {
		return turn;
	}
	
	public void setTurn(Chip.Type turn) {
		this.turn = turn;	
	}
	
	public void checkIfSomeoneWon() {
//		if(removedWhiteChip >= 6 && removedBlackChip >= 6) {
//			String won = whiteScore > blackScore ? "WHITE WON" : "BLACK WON";
//			won = whiteScore == blackScore ? "DRAW" : won;
//			screen.won(won);
//			L.wtf("someone won!!");
//		}
		String won = "";
		if(whiteScore >= 800 ){
			won = "WHITE WON";
			screen.won(won);
		} else if(blackScore >= 800 ){
			won = "BLACK WON";
			screen.won(won);
		}
	}
	
	public void setGameScreen(GameScreen screen) {
		this.screen = screen;
	}
	
	public void setTutorialScreen(TutorialScreen tutorialScreen) {
		this.tutorialScreen = tutorialScreen;
	}
	
	public void setBoard(String[][] board) {
		this.board = board;
	}
	
	public ArrayList<Chip> getChips() {
		return chips;
	}
	
	public ArrayList<Chip> getChips(Chip.Type type) {
		ArrayList<Chip> certainChips = new ArrayList<Chip>();
		for(Chip chip: chips) {
			if(chip.getType() == type)
				certainChips.add(chip);
		}
		return certainChips;
	}
	
	public Slot[][] getSlots() {
		return slots;
	}
	
	public ArrayList<Chip> getSelected() {
		return selected;
	}
	
	public ArrayList<Neighbors> getNeighbors() {
		return neighbors;
	}
	
	public Chip.Neighbors getOppositeNeighbor(Chip.Neighbors neighbor) {
		switch (neighbor) {
		case EAST:
			return Neighbors.WEST;
		case WEST:
			return Neighbors.EAST;
		case NORTH_EAST:
			return Neighbors.SOUTH_WEST;
		case NORTH_WEST:
			return Neighbors.SOUTH_EAST;
		case SOUTH_EAST:
			return Neighbors.NORTH_WEST;
		case SOUTH_WEST:
			return Neighbors.NORTH_EAST;
		default:
			break;
		}
		return null;
		
	}

}
