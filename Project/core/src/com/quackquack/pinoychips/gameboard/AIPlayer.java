package com.quackquack.pinoychips.gameboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import com.game.framework.utils.L;
import com.quackquack.pinoychips.gameboard.Chip.Neighbors;
import com.quackquack.pinoychips.gameboard.Chip.Type;

public class AIPlayer {
	
	private GameBoard board;
	public Type type;
	
	public class PushTurn {
		public ArrayList<Chip> selected;
		public Chip chipToPush;
		public ArrayList<Chip> neighborChips;
	}
	
	public class MoveTurn {
		public ArrayList<Chip> selected;
		public ArrayList<Slot> targetSlots;
	}

	public AIPlayer(GameBoard board, Chip.Type type) {
		this.board = board;
		this.type = type;
		
	}
	
	public void turn() {
		//get available push turns
		ArrayList<PushTurn> pushTurns = getPushTurns();
//		L.wtf("push turns "+pushTurns.size());
		if(pushTurns.size() > 0) {
			int rand = (int)(Math.random() * pushTurns.size());
			PushTurn turn = pushTurns.get(rand);
			Slot[][] slots = board.getSlots();
			Slot slotTarget = slots[turn.chipToPush.getRow()][turn.chipToPush.getColumn()];
			Neighbors direction = board.moveSelected(slotTarget, turn.selected);
			board.getSelected().addAll(turn.selected);
			board.pushNeighbors(turn.chipToPush, direction);
			board.getSelected().clear();
			board.getNeighbors().clear();
		} else {
			ArrayList<MoveTurn> moveTurns = getMoveTurn();
			int rand = (int)(Math.random() * moveTurns.size());
			MoveTurn moveTurn = moveTurns.get(rand);
			board.getSelected().addAll(moveTurn.selected);
			
			rand = (int)(Math.random() * moveTurn.targetSlots.size());
			Slot targetSlot = moveTurn.targetSlots.get(rand);
			
			board.moveSelected(targetSlot, moveTurn.selected);
			board.getSelected().clear();
			board.getNeighbors().clear();
		}
		
		
	}

	private ArrayList<PushTurn> getPushTurns() {
		ArrayList<Chip> chips = board.getChips(type);
		
		//get chips that has opposite type neighbor
		ArrayList<Chip> chipsWithOtherTypeNeighbor = getChipsWithOtherTypeNeighbor(chips);
		
		//get push turns
		ArrayList<PushTurn> pushTurns = generatePushTurns(chipsWithOtherTypeNeighbor);
		return pushTurns;
	}

	private ArrayList<PushTurn> generatePushTurns(
			ArrayList<Chip> chipsWithOtherTypeNeighbor) {
		ArrayList<PushTurn> pushTurns = new ArrayList<PushTurn>();
		
		ArrayList<Neighbors> neighbors = new ArrayList<Chip.Neighbors>(Arrays.asList(Neighbors.values()));
		for(Chip chip : chipsWithOtherTypeNeighbor) {
			for(Neighbors neighbor : neighbors) {
				Chip neighborChip = board.getNeighborChip(neighbor, chip);
				if(neighborChip!=null && neighborChip.getType() != type) {
					board.getSelected().add(chip);
					board.getNeighbors().add(neighbor);
					ArrayList<Chip> neighborChips = board.getSlantNeighborChips(neighborChip);
					ArrayList<Slot> availableSlotLocationOfNeighbors = board.getAvailableSlotLocation(neighborChips);
					
					board.getSelected().clear();
					board.getNeighbors().clear();
					
					board.getSelected().add(neighborChip);
					board.getNeighbors().add(board.getOppositeNeighbor(neighbor));
					ArrayList<Chip> selectedChips = board.getSlantNeighborChips(chip);
					
					board.getSelected().clear();
					board.getNeighbors().clear();
					
					if(selectedChips.size() > neighborChips.size() && availableSlotLocationOfNeighbors.size() > 0) {
//						L.wtf("selected size "+selectedChips.size()+" n-size "+neighborChips.size()+" neighbor "+neighbor.toString());
//						L.wtf("chip r : "+chip.getRow()+" c : "+chip.getColumn());
//						L.wtf("n-chip r : "+neighborChip.getRow()+" c : "+neighborChip.getColumn());
						PushTurn turn = new PushTurn();
						turn.selected = selectedChips;
						turn.chipToPush = neighborChip;
						turn.neighborChips = neighborChips;
						pushTurns.add(turn);
					}
				}
			}
			
		}
		
		return pushTurns;
	}

	private ArrayList<Chip> getChipsWithOtherTypeNeighbor(ArrayList<Chip> chips) {
		ArrayList<Chip> otherTypeNeighbor = new ArrayList<Chip>();
		ArrayList<Neighbors> neighbors = new ArrayList<Chip.Neighbors>(Arrays.asList(Neighbors.values()));
		for(int i=0;i<chips.size();i++) {
			Chip chip = chips.get(i);
			for(Neighbors neighbor : neighbors) {
				Chip neighborChip = board.getNeighborChip(neighbor, chip);
				if(neighborChip!=null && neighborChip.getType() != type) {
					if(!otherTypeNeighbor.contains(chip))//just once
						otherTypeNeighbor.add(chip);
				}
			}
		}
		return otherTypeNeighbor;
	}
	
	private ArrayList<MoveTurn> getMoveTurn() {
		ArrayList<MoveTurn> moveTurns = new ArrayList<AIPlayer.MoveTurn>();
		
		ArrayList<Chip> chips = board.getChips(type);
		//get  all movable single chips
		ArrayList<Chip> movableChips = getMovableChips(chips);
		//create single Move turns
		ArrayList<MoveTurn> singleMoveTurn = createSingleMoveTurn(movableChips);
		
		//get all movable Slant 
		ArrayList<MoveTurn> slantMoveTurn = getMovableSlantTurns(movableChips);
		
		moveTurns.addAll(slantMoveTurn);
		moveTurns.addAll(singleMoveTurn);
		return moveTurns;
	}

	private ArrayList<MoveTurn> createSingleMoveTurn(ArrayList<Chip> chips) {
		
		ArrayList<MoveTurn> moveTurns = new ArrayList<AIPlayer.MoveTurn>();
		for(Chip chip : chips) {
			
			ArrayList<Slot> availableLocation = board.getAvailableLocation(chip);
			if(availableLocation.size()>0) {
				MoveTurn turn = new MoveTurn();
				ArrayList<Chip> selected = new ArrayList<Chip>();
				selected.add(chip);
				turn.selected = selected;
				turn.targetSlots = availableLocation;
				moveTurns.add(turn);
				//L.wtf("location size "+availableLocation.size());
			}
		}
		return moveTurns;
	}

	private ArrayList<MoveTurn> getMovableSlantTurns(ArrayList<Chip> chips) {
		ArrayList<MoveTurn> moveTurns = new ArrayList<MoveTurn>();
		ArrayList<Neighbors> neighbors = new ArrayList<Chip.Neighbors>(Arrays.asList(Neighbors.values()));
		for(int i=0;i<chips.size();i++) {
			Chip chip = chips.get(i);
			for(Neighbors neighbor : neighbors) {
				Chip neighborChip = board.getNeighborChip(neighbor, chip);
				if(neighborChip != null && neighborChip.getType() == chip.getType()) {
					board.getSelected().add(chip);
					board.getNeighbors().add(neighbor);
					ArrayList<Chip> neighborChips = board.getSlantNeighborChips(neighborChip,2);
					neighborChips.add(chip);
					
					board.getSelected().clear();
					board.getNeighbors().clear();
					
					if(neighborChips.size() > 1) {
						board.getNeighbors().add(neighbor);
						board.getNeighbors().add(board.getOppositeNeighbor(neighbor));
						ArrayList<Slot> availableLocation = board.getAvailableSlotLocation(neighborChips);
						removeNotVisibleLocation(availableLocation);
						ArrayList<Slot> availableLeapLocation = board.getAvailableLeapLocation(neighborChips);
						availableLocation.addAll(availableLeapLocation);
						board.getNeighbors().clear();
						
//						L.wtf(chip.getRow()+"/"+chip.getColumn()+" "+neighbor.toString()+" neighbor size "+neighborChips.size()+" available location "+availableLocation.size());
						for(Slot slot:availableLocation){
							L.wtf("r: "+slot.getRow()+" c: "+slot.getColumn());
						}
						if(availableLocation.size() > 0) {
							MoveTurn turn = new MoveTurn();
							turn.selected = neighborChips;
							turn.targetSlots = availableLocation;
							moveTurns.add(turn);
						}
					}
				}
			}
		}
		
		return moveTurns;
	}

	private void removeNotVisibleLocation(ArrayList<Slot> availableLocation) {
		Iterator<Slot> i = availableLocation.iterator();
		while (i.hasNext()) {
		   Slot slot = i.next();
		   
		   if(!slot.isVisible())
			   i.remove();
		}
	}

	private ArrayList<Chip> getMovableChips(ArrayList<Chip> chips) {
		ArrayList<Chip> movableChip = new ArrayList<Chip>();
		for(int i=0;i<chips.size();i++) {
			Chip chip = chips.get(i);
			ArrayList<Slot> availableLocation = board.getAvailableLocation(chip);
			if(availableLocation.size()>0) {
				movableChip.add(chip);
			}
		}
		return movableChip;
	}

}
