package com.quackquack.pinoychips.gameboard;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class ChipListener extends InputListener {
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer,
			int button) {
		onTap();
		return super.touchDown(event, x, y, pointer, button);
	}
	
	public void onTap() {
		
	}

}
