package com.quackquack.pinoychips.screen;

import java.util.ArrayList;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Cubic;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.display.ui.NinePatchButton;
import com.game.framework.display.ui.NinePatchDisplay;
import com.game.framework.listeners.ActorClickListener;
import com.game.framework.listeners.ActorDragListener;
import com.game.framework.utils.ActorTweenAccessor;
import com.game.framework.utils.L;
import com.quackquack.pinoychips.Assets;
import com.sun.org.apache.bcel.internal.generic.GETSTATIC;

public class PreGameScreen extends AbstractScreen {
	
	public static final int GAME_MODE_PLAYER_VS_AI = 0;
	public static final int GAME_MODE_PLAYER_VS_PLAYER = 1;
	public static final int GAME_MODE_TUTORIAL = 2;
	
	private ArrayList<NinePatchDisplay> bgs;
	private ArrayList<NinePatchDisplay> arrs;
	private ArrayList<NinePatchDisplay> gameModes;
	private int bgIndex = 0;
	private int arrangementIndex = 0;
	private int gameModeIndex = 0;
	private Group bgGroup;
	private Group arrsGroup;
	private Group gameModeGroup;
	
	public PreGameScreen() {
		super();
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion bg = atlas.findRegion("bg");
		
		DisplayObject bgDisplay = new DisplayObject(bg);
		addActor(bgDisplay);
		
		BitmapFont headerFont = Assets.getInstance().generateFont(50, Assets.getInstance().getDefaultAllCharSet());
		DisplayText headerText = new DisplayText("CUSTOMIZE GAME",headerFont);
		addActor(headerText);
		headerText.setPosition((stage.getWidth() - headerText.getTextWidth())/2, stage.getHeight() - 20 - headerText.getTextHeight());
		
		NinePatch btnUpTexture = atlas.createPatch("button");
		BitmapFont font = Assets.getInstance().generateFont(30, Assets.getInstance().getDefaultAllCharSet());
		
		NinePatchButton backB = createNinePatchButton(btnUpTexture, 150, 40);
		addText(backB, "BACK", font);
		backB.setPosition(11,14);
		addActor(backB);
		backB.setClickListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				switchScreen(new MainMenuScreen());
			}
		});
		
		NinePatchButton startB = createNinePatchButton(btnUpTexture, 150, 40);
		addText(startB, "START", font);
		startB.setPosition(312,13);
		addActor(startB);
		startB.setClickListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				switchScreen(new GameScreen(bgIndex,arrangementIndex,gameModeIndex));
			}
		});
		
		createBackgrounds(atlas,btnUpTexture);
		createArrangement(atlas,btnUpTexture);
		createGameModes(atlas,btnUpTexture,font);
		
		TextureRegion arrow = atlas.findRegion("arrow");
		NinePatchButton bgLeftB = createNinePatchButton(btnUpTexture, 60, 60);
		DisplayObject img =  addImage(bgLeftB, arrow);
		img.setFlipX(true);
		bgLeftB.setPosition(24,534);
		addActor(bgLeftB);
		bgLeftB.setClickListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				bgIndex -=1;
				if(bgIndex < 0) bgIndex = 0;
				tween(bgGroup,bgIndex);
			}
		});
		
		NinePatchButton bgRightB = createNinePatchButton(btnUpTexture, 60, 60);
		addImage(bgRightB, arrow);
		bgRightB.setPosition(400,534);
		addActor(bgRightB);
		bgRightB.setClickListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				bgIndex +=1;
				if(bgIndex > bgs.size() - 1) bgIndex = bgs.size() - 1; 
				tween(bgGroup,bgIndex);
			}
		});
		
		NinePatchButton arrangementLeftB = createNinePatchButton(btnUpTexture, 60, 60);
		DisplayObject img2 =  addImage(arrangementLeftB, arrow);
		img2.setFlipX(true);
		arrangementLeftB.setPosition(24,304);
		addActor(arrangementLeftB);
		arrangementLeftB.setClickListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				arrangementIndex -=1;
				if(arrangementIndex < 0) arrangementIndex = 0;
				tween(arrsGroup,arrangementIndex);
			}
		});
		
		NinePatchButton arrangementRightB = createNinePatchButton(btnUpTexture, 60, 60);
		addImage(arrangementRightB, arrow);
		arrangementRightB.setPosition(400,304);
		addActor(arrangementRightB);
		arrangementRightB.setClickListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				arrangementIndex +=1;
				if(arrangementIndex > arrs.size() - 1) arrangementIndex = arrs.size() - 1; 
				tween(arrsGroup,arrangementIndex);
			}
		});
		
		NinePatchButton gameModeLeftB = createNinePatchButton(btnUpTexture, 60, 60);
		DisplayObject img3 =  addImage(gameModeLeftB, arrow);
		img3.setFlipX(true);
		gameModeLeftB.setPosition(24,130);
		addActor(gameModeLeftB);
		gameModeLeftB.setClickListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				gameModeIndex -=1;
				if(gameModeIndex < 0) gameModeIndex = 0;
				tween(gameModeGroup,gameModeIndex);
			}
		});
		
		NinePatchButton gameModeRightB = createNinePatchButton(btnUpTexture, 60, 60);
		addImage(gameModeRightB, arrow);
		gameModeRightB.setPosition(400,130);
		addActor(gameModeRightB);
		gameModeRightB.setClickListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				gameModeIndex +=1;
				if(gameModeIndex > gameModes.size() - 1) gameModeIndex = gameModes.size() - 1; 
				tween(gameModeGroup,gameModeIndex);
			}
		});
		
		initScreen();
	}
	
	private void createGameModes(TextureAtlas atlas, NinePatch ninePatch,BitmapFont font) {
		gameModes = new ArrayList<NinePatchDisplay>();
		gameModeGroup = new Group();
		addActor(gameModeGroup);
		
		NinePatchButton playerVSAI = createNinePatchButton(ninePatch, 250, 130);
		playerVSAI.setTouchable(Touchable.disabled);
		addText(playerVSAI, "PLAYER\nVS\nAI", font);
		playerVSAI.setPosition((stage.getWidth() - playerVSAI.getWidth())/2,100);
		addActor(playerVSAI);
		gameModeGroup.addActor(playerVSAI);
		gameModes.add(playerVSAI);
		
		NinePatchButton playerVSPlayer = createNinePatchButton(ninePatch, 250, 130);
		playerVSPlayer.setTouchable(Touchable.disabled);
		addText(playerVSPlayer, "PLAYER\nVS\nPLAYER", font);
		playerVSPlayer.setPosition((stage.getWidth() - playerVSAI.getWidth())/2 + stage.getWidth(),100);
		addActor(playerVSPlayer);
		gameModeGroup.addActor(playerVSPlayer);
		gameModes.add(playerVSPlayer);
	}

	private void createArrangement(TextureAtlas atlas, NinePatch ninePatch) {
		arrs = new ArrayList<NinePatchDisplay>();
		arrsGroup = new Group();
		addActor(arrsGroup);
		
		for(int i=0;i<2;i++){
			String bgName = "arrangement"+(i+1);
			TextureRegion texture = atlas.findRegion(bgName);
			NinePatchDisplay display = new NinePatchDisplay(ninePatch, 170, 170);
			addImage(display, texture);
			display.setPosition(stage.getWidth() * i + (stage.getWidth() - display.getWidth())/2,stage.getHeight() - 380 - display.getHeight() );
			arrsGroup.addActor(display);
			arrs.add(display);
		}
	}

	private void tween(Group group,int index) {
		L.wtf("index "+(index * stage.getWidth()));
		Tween.to(group, ActorTweenAccessor.POSITION_X, .5f)
		.target(-index * stage.getWidth())
		.ease(Cubic.IN)
		.start(manager)
		.setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
			}
		});
	}
	
	private void createBackgrounds(TextureAtlas atlas,NinePatch ninePatch) {
		bgs = new ArrayList<NinePatchDisplay>();
		bgGroup = new Group();
		addActor(bgGroup);
		
		for(int i=0;i<5;i++){
			String bgName = "bg"+(i+1);
			TextureRegion texture = atlas.findRegion(bgName);
			NinePatchDisplay display = new NinePatchDisplay(ninePatch, 140, 240);
			addImage(display, texture,100,200);
			display.setPosition(stage.getWidth() * i + (stage.getWidth() - display.getWidth())/2,stage.getHeight() - 100 - display.getHeight() );
			bgGroup.addActor(display);
			bgs.add(display);
		}
	}

	private NinePatchButton createNinePatchButton(NinePatch ninepatch,float width,float height) {
		NinePatchButton btn = new NinePatchButton(ninepatch, ninepatch, width, height);
		btn.setTweenManager(manager);
		addActor(btn);
//		btn.addListener(new ActorDragListener());
		return btn;
	}
	
	private void addText(NinePatchButton btn,String text,BitmapFont font) {
		DisplayText display = new DisplayText(text, font);
		display.autoResize = false;
		display.setAlignment(HAlignment.CENTER);
		display.setPreferedWidth(btn.getWidth());
		display.setY(5);
		btn.addActor(display);
		//display.isDebug = true;
	}
	
	private DisplayObject addImage(NinePatchDisplay ninePatch,TextureRegion texture) {
		return addImage(ninePatch, texture, texture.getRegionWidth(), texture.getRegionHeight());
	}
	
	private DisplayObject addImage(NinePatchDisplay ninePatch,TextureRegion texture,int width,int height) {
		DisplayObject display = new DisplayObject(texture);
		ninePatch.addActor(display);
		display.setSize(width, height);
		display.setPosition((ninePatch.getScaleWidth() - display.getWidth())/2, (ninePatch.getScaleHeight() - display.getHeight())/2);
		return display;
	}

}
