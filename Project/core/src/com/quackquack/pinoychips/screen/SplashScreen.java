package com.quackquack.pinoychips.screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.listeners.ActorDragListener;
import com.quackquack.pinoychips.Assets;



public class SplashScreen extends AbstractScreen {
	
	private int state = 0;
	
	public SplashScreen() {
		super();
		
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion bg = atlas.findRegion("bg");
		TextureRegion logo = atlas.findRegion("logo");
		BitmapFont font = Assets.getInstance().generateFont(25, Assets.getInstance().getDefaultAllCharSet());
		DisplayObject bgDisplay = new DisplayObject(bg);
		addActor(bgDisplay);
		
		DisplayObject logoDisplay = new DisplayObject(logo);
		logoDisplay.setSize(150, 150);
		logoDisplay.setPosition(159,500);
//		logoDisplay.addListener(new ActorDragListener());
		addActor(logoDisplay);
		
		String text = "Developed by:\nJasmine S. Enopia"
				+ "\nDanica Denice E. Uayan "
				+ "\nJonalyn C. Pascua "
				+ "\n\n"
				+ "Adviser:\nGrant B. Cornell"
				+ "\n\n"
				+ "Copyright\n2015"
				+ "\n\n"
				+ "BSCS 4A";
		DisplayText textDisplay = new DisplayText(text,font);
		textDisplay.setColor(Color.BLACK);
		textDisplay.setPosition(40, 470 - textDisplay.getTextHeight());
		addActor(textDisplay);
		textDisplay.setAlignment(HAlignment.CENTER);
		textDisplay.setPreferedWidth(400);
//		textDisplay.addListener(new ActorDragListener());
		
		stage.addAction(new Action() {
			float time = 0;
			@Override
			public boolean act(float delta) {
				time += delta;
				if(time >= 6) {//3 sec 
					switchScreen(new MainMenuScreen());
					return true;
				}
				return false;
			}
		});
		
		initScreen();
	}

}
