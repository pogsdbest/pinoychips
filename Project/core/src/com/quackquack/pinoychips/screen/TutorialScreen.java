package com.quackquack.pinoychips.screen;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.display.ui.NinePatchDisplay;
import com.game.framework.listeners.BackKeyListener;
import com.game.framework.utils.BackKeyCatcher;
import com.game.framework.utils.L;
import com.quackquack.pinoychips.Assets;
import com.quackquack.pinoychips.gameboard.BoardArrangement;
import com.quackquack.pinoychips.gameboard.Chip;
import com.quackquack.pinoychips.gameboard.ChipListener;
import com.quackquack.pinoychips.gameboard.GameBoard;
import com.quackquack.pinoychips.gameboard.Slot;
import com.quackquack.pinoychips.tutorial.GameProcess;
import com.quackquack.pinoychips.tutorial.TapCallback;


public class TutorialScreen extends AbstractScreen {
	
	private GameBoard gameboard;
	private NinePatchDisplay dialogueWindow;
	private DisplayText text;
	private ArrayList<GameProcess> executeProcess;
	public boolean isProccessing;
	private DisplayText nextText;
	public boolean escapeTutorial;

	public TutorialScreen() {
		super();

		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		int rnd = (int)(Math.random() * 6);
		TextureRegion bgTexture = atlas.findRegion("bg"+rnd);
		
		NinePatch ninePatch = atlas.createPatch("button");
		BitmapFont font = Assets.getInstance().generateFont(18, Assets.getInstance().getDefaultAllCharSet());
		
		DisplayObject bgDisplay = new DisplayObject(bgTexture);
		//addActor(bgDisplay);
		
		gameboard = new GameBoard(0,PreGameScreen.GAME_MODE_TUTORIAL);
		gameboard.setPosition(-60, 725);
		gameboard.setTweenManager(manager);
		gameboard.init();
		gameboard.setTutorialScreen(this);
		addActor(gameboard);
		
		DisplayObject blocker = new DisplayObject();
		blocker.setSize(stage.getWidth(), stage.getHeight());
		addActor(blocker);
		blocker.setAlpha(0);
		
		dialogueWindow = new NinePatchDisplay(ninePatch, 400, 200);
		dialogueWindow.setPosition(40, 20);
		dialogueWindow.setAlpha(.75f);
//		dialogueWindow.addListener(new ActorDragListener());
		addActor(dialogueWindow);
		
		nextText = new DisplayText("Next",font);
		nextText.setPosition(dialogueWindow.getWidth() - nextText.getTextWidth() - 10, 10);
		dialogueWindow.addActor(nextText);
		
		text = new DisplayText("",font);
		text.setPreferedWidth(dialogueWindow.getWidth() - 20);
		dialogueWindow.addActor(text);
//		text.isDebug = true;
		
		executeProcess = new ArrayList<GameProcess>();
		
		showIntro();
		
		BackKeyCatcher.getInstance().listener = new BackKeyListener() {
			
			@Override
			public void backPressed() {
				escapeTutorial = true;
			}
		};
		
		dialogueWindow.addAction(new Action() {
			
			@Override
			public boolean act(float delta) {
				if(!isProccessing) {
					if(escapeTutorial) {
						switchScreen(new MainMenuScreen());
						return true;
					}
				}
				return false;
			}
		});
		
		initScreen();
	}
	
	private void showIntro() {
		gameboard.setBoard(BoardArrangement.board0);
		gameboard.designBoard();
		
		
		
		showDialogue( "Peragon is a board game for 2 players.\n "
				     + "Each player has 14 chips of the same color of his command,"
				     + " one player owns black and  the other owns white."
				     + " Here is the initial position at the beginning of the game.",new TapCallback() {
			@Override
			public void tap() {
				showSingleMove();
			}
		});
		isProccessing = false;
		nextText.setVisible(true);
	}
	
	private void showSingleMove() {
		executeProcess.clear();
		gameboard.setBoard(BoardArrangement.board1);
		gameboard.designBoard();
		gameboard.setTurn(Chip.Type.WHITE);
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				Chip chip = gameboard.getChips().get(0);
				ChipListener listener = (ChipListener)chip.getListeners().get(0);
				listener.onTap();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				final Chip chip = gameboard.getChips().get(0);
				final ArrayList<Slot> availableLocation = gameboard.getAvailableLocation(chip);
				gameboard.removeListeners(availableLocation);
				chip.isSelected = false;
				chip.moveTween(availableLocation.get(0),null);
			}
		});
		startProcesses();
		showDialogue("In order  to succeed, players take turns to move their chips. "
				     + "Take a look at how to move a single chip. You can move a single "
				     + "chips in any direction. The only condition is that the destination "
				     + "cell should be empty.",new TapCallback() {
			@Override
			public void tap() {
				showSelectGroup();
			}
		});
		
	}
	
	private void showSelectGroup() {
		isProccessing = true;
		executeProcess.clear();
		gameboard.setBoard(BoardArrangement.board2);
		gameboard.designBoard();
		gameboard.setTurn(Chip.Type.WHITE);
		
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(8, 4).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(8, 5).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(8, 6).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(8, 4).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(4, 5).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(3, 5).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(4, 5).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(3, 7).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(3, 8).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(3, 7).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(3, 8).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(4, 9).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(3, 8).getListeners().get(0)).onTap();
			}
		});
		
		startProcesses();
		showDialogue("You can also move a group of chips. You can only move "
					+ "a maximum of 3 chips. And  you can only select and move "
					+ "them if the chips are on the same line.",new TapCallback() {
			@Override
			public void tap() {
				if(isProccessing) return;
				showLeapMove();
			}
		});
	}
	
	private void showLeapMove() {
		executeProcess.clear();
		gameboard.setBoard(BoardArrangement.board3);
		gameboard.designBoard();
		gameboard.setTurn(Chip.Type.WHITE);
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(3, 5).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(4, 5).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(5, 4).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				ArrayList<Chip> selected = gameboard.getSelected();
				
				ArrayList<Slot> availableLocation = gameboard.getAvailableLeapLocation(selected);
				availableLocation.addAll(gameboard.getAvailableSlotLocation(selected));
				Slot slot = availableLocation.get(0);
				gameboard.moveSelected(slot, selected);
				gameboard.removeListeners(availableLocation);
			}
		});
		startProcesses();
		showDialogue("You can perform this move in any of the four side directions. "
					+ "Each of the destination cells must be empty. ",new TapCallback() {
			@Override
			public void tap() {
				showPushMove();
			}
		});
	}
	
	private void showPushMove() {
		executeProcess.clear();
		gameboard.setBoard(BoardArrangement.board4);
		gameboard.designBoard();
		gameboard.setTurn(Chip.Type.WHITE);
		
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(7, 4).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(6, 5).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(5, 5).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				ArrayList<Chip> selected = gameboard.getSelected();
				ArrayList<Slot> availableLocation = gameboard.getAvailableSlotLocation(selected);
				Slot slot = availableLocation.get(0);
				availableLocation.addAll(gameboard.getAvailableLeapLocation(selected));
				gameboard.moveSelected(slot, selected);
				gameboard.removeListeners(availableLocation);
				gameboard.getSelected().clear();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(7, 6).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(6, 6).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(5, 5).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				ArrayList<Chip> selected = gameboard.getSelected();
				ArrayList<Slot> availableLocation = gameboard.getAvailableSlotLocation(selected);
				Slot slot = availableLocation.get(0);
				availableLocation.addAll(gameboard.getAvailableLeapLocation(selected));
				gameboard.moveSelected(slot, selected);
				gameboard.removeListeners(availableLocation);
			}
		});
		
		startProcesses();
		showDialogue("Another kind of move is when you push one or two friendly "
					+ "chips forward. The destination cell should be also empty.",new TapCallback() {
			@Override
			public void tap() {
				showPushOpposite();
			}
		});
	}
	
	private void showPushOpposite() {
		executeProcess.clear();
		gameboard.setBoard(BoardArrangement.board5);
		gameboard.designBoard();
		gameboard.setTurn(Chip.Type.WHITE);
		
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(3, 4).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(4, 5).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(5, 5).getListeners().get(0)).onTap();
			}
		});
		GameProcess p1 = new GameProcess() {
			@Override
			public void execute() {
				ArrayList<Chip> selected = gameboard.getSelected();
				ArrayList<Slot> availableLocation = gameboard.getAvailableLeapLocation(selected);
				availableLocation.addAll(gameboard.getAvailableSlotLocation(selected));
				gameboard.removeListeners(availableLocation);
				ArrayList<Chip> availablePushLocation = gameboard.getAvailablePushLocation(selected);
				Chip chipToPush = availablePushLocation.get(0);
				chipToPush.getPushListener().onTap();
				
			}
		};
		executeProcess.add(p1);
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				gameboard.setTurn(Chip.Type.BLACK);
				((ChipListener)gameboard.getChip(7, 4).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(6, 5).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(p1);
		
		startProcesses();
		showDialogue("By performing a push move, you can push the chip/s of your opponent "
					+ "Remember that the number of your own chips must be greater"
					+ " than the number of opponent marbles being pushed.",new TapCallback() {
			@Override
			public void tap() {
				showCantPush();
			}
		});
	}
	
	private void showCantPush() {
		executeProcess.clear();
		gameboard.setBoard(BoardArrangement.board6);
		gameboard.designBoard();
		gameboard.setTurn(Chip.Type.WHITE);
		
		
		startProcesses();
		showDialogue("In this position black chips cannot push white chips "
					+ "though they are greater in number. Remember  that you"
					+ " can only move a maximum of 3 chips.",new TapCallback() {
			@Override
			public void tap() {
				showDestroyingOneAnother();
			}
		});
		isProccessing = false;
		nextText.setVisible(true);
	}
	
	private void showDestroyingOneAnother() {
		executeProcess.clear();
		gameboard.setBoard(BoardArrangement.board7);
		gameboard.designBoard();
		gameboard.setTurn(Chip.Type.BLACK);
		
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(7, 8).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(6, 9).getListeners().get(0)).onTap();
			}
		});
		GameProcess p1 = new GameProcess() {
			@Override
			public void execute() {
				ArrayList<Chip> selected = gameboard.getSelected();
				ArrayList<Slot> availableLocation = gameboard.getAvailableLeapLocation(selected);
				availableLocation.addAll(gameboard.getAvailableSlotLocation(selected));
				gameboard.removeListeners(availableLocation);
				ArrayList<Chip> availablePushLocation = gameboard.getAvailablePushLocation(selected);
				Chip chipToPush = availablePushLocation.get(0);
				chipToPush.getPushListener().onTap();
				gameboard.setTurn(Chip.Type.WHITE);
			}
		};
		executeProcess.add(p1);
		executeProcess.add(new GameProcess() {
			@Override
			public void execute() {
				((ChipListener)gameboard.getChip(5, 7).getListeners().get(0)).onTap();
				((ChipListener)gameboard.getChip(5, 8).getListeners().get(0)).onTap();
			}
		});
		executeProcess.add(p1);
		
		startProcesses();
		showDialogue("If you push a chip of your opponent out of the board, "
					+ "it becomes captured. In order  to win Peragon, the player"
					+ " must garner a total score of 800.",new TapCallback() {
			@Override
			public void tap() {
				showCompleteTutorial();
			}
		});
	}
	
	private void showCompleteTutorial() {
		executeProcess.clear();
		gameboard.setBoard(BoardArrangement.board8);
		gameboard.designBoard();
		gameboard.setTurn(Chip.Type.BLACK);
		
		
		
		startProcesses();
		showDialogue("Congratulations!, you have completed the tutorial!."
					+ " Good luck and Have Fun!",new TapCallback() {
			@Override
			public void tap() {
				switchScreen(new MainMenuScreen());
			}
		});
		isProccessing = false;
		nextText.setVisible(true);
	}
	
	private void showDialogue(String message,final TapCallback callback) {
		isProccessing = true;
		nextText.setVisible(false);
		text.setText(message);
		text.setPosition(10, dialogueWindow.getHeight() - text.getTextHeight() - 28);
		
		dialogueWindow.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				
				if(isProccessing) return super.touchDown(event, x, y, pointer, button);
				
				dialogueWindow.removeListener(this);
				callback.tap();
				
				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}
	
	private void startProcesses() {
		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				for(GameProcess process : executeProcess) {
					process.execute();
					try{
						Thread.sleep(1000);
					} catch(Exception e){
						e.printStackTrace();
						break;
					}
				}
				L.wtf("is Processisng done");
				isProccessing = false;
				nextText.setVisible(true);
			}
		});
		t.start();
		
	}

}
