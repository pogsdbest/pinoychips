package com.quackquack.pinoychips.screen;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.equations.Cubic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.display.ui.NinePatchButton;
import com.game.framework.display.ui.NinePatchDisplay;
import com.game.framework.listeners.ActorClickListener;
import com.game.framework.listeners.ActorDragListener;
import com.game.framework.utils.ActorTweenAccessor;
import com.quackquack.pinoychips.Assets;



public class MainMenuScreen extends AbstractScreen {

	private NinePatchDisplay window;

	public MainMenuScreen() {
		super();
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		NinePatch btnUpTexture = atlas.createPatch("button");
		TextureRegion title = atlas.findRegion("title");
		TextureRegion bg = atlas.findRegion("bg");
		TextureRegion logo = atlas.findRegion("logo");
		BitmapFont font = Assets.getInstance().generateFont(30, Assets.getInstance().getDefaultAllCharSet());
		
		DisplayObject bgDisplay = new DisplayObject(bg);
		addActor(bgDisplay);
		
		DisplayObject logoDisplay = new DisplayObject(logo);
		logoDisplay.setPosition((stage.getWidth() - logoDisplay.getWidth())/2, (stage.getHeight()-logoDisplay.getHeight())/2);
		addActor(logoDisplay);
		logoDisplay.setOrigin(logoDisplay.getWidth()/2, logoDisplay.getHeight()/2);
		Tween.to(logoDisplay,ActorTweenAccessor.ROTATION,5f)
				.target(180f)
				.ease(Cubic.IN)
				.repeatYoyo(Tween.INFINITY, .5f)
				.start(manager);
		
		DisplayObject titleDisplay = new DisplayObject(title);
		titleDisplay.setPosition(73,672);
		addActor(titleDisplay);
		
		NinePatchButton startB = createNinePatchButton(btnUpTexture, 150, 40);
		addText(startB, "PLAY", font);
		startB.setPosition(154,543);
		addActor(startB);
		startB.setClickListener(new ActorClickListener(){
			@Override
			public void clicked(DisplayObject object) {
				switchScreen(new PreGameScreen());
			}
		});
		
		
		NinePatchButton tutorialB = createNinePatchButton(btnUpTexture, 200, 40);
		addText(tutorialB, "TUTORIAL", font);
		tutorialB.setPosition(154-25,442);
		addActor(tutorialB);
		tutorialB.setClickListener(new ActorClickListener(){
			@Override
			public void clicked(DisplayObject object) {
				switchScreen(new TutorialScreen());
			}
		});
		
		NinePatchButton optionB = createNinePatchButton(btnUpTexture, 150, 40);
		addText(optionB, "CREDITS", font);
		optionB.setPosition(154,335); //154 - 25,335
		addActor(optionB);
		optionB.setClickListener(new ActorClickListener(){
			@Override
			public void clicked(DisplayObject object) {
				showOption();
			}
		});
		
		NinePatchButton exitB = createNinePatchButton(btnUpTexture, 150, 40);
		addText(exitB, "QUIT", font);
		exitB.setPosition(154,223);
		addActor(exitB);
		exitB.setClickListener(new ActorClickListener() {
			
			@Override
			public void clicked(DisplayObject object) {
				Gdx.app.exit();
			}
		});
		
		createOptionWindow();
		
		Music music = Assets.getInstance().get("sfx/bgm.mp3");
		music.setLooping(true);
		Assets.getInstance().playMusic("sfx/bgm.mp3");
		initScreen();
	}
	
	private void createOptionWindow() {
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		NinePatch ninePatch = atlas.createPatch("button");
		
		window = new NinePatchDisplay(ninePatch, 400, 400);
		window.setPosition((stage.getWidth() - window.getWidth())/2, (stage.getHeight() - window.getHeight())/2);
		addActor(window);
		
		BitmapFont font = Assets.getInstance().generateFont(22, Assets.getInstance().getDefaultAllCharSet());
		DisplayText text = new DisplayText("", font);
		text.setPreferedWidth(window.getWidth() - 20);
		text.setAlignment(HAlignment.CENTER);
		text.addListener(new ActorDragListener());
		text.setText("Developers:\nEnopia Jasmine S."
					+ "\nUayan Danica Denice E."
					+ "\nPascua Jonalyn C."
					+ "\n\n"
					+ "Music:\nRyan Ancona- Sweet"
					+ "\n\nThemes:\nGoogle.com"
					+ "\n\nCopyright\n2015");
		text.setPosition(10,window.getHeight() -  text.getTextHeight()- 30 );
		window.addActor(text);
		
		window.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				window.setVisible(false);
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		
		final TextureRegion sound = atlas.findRegion("sound");
		final TextureRegion soundOff = atlas.findRegion("soundoff");
		final DisplayObject soundDisplay = new DisplayObject(sound);
		soundDisplay.setSize(50, 50);
		soundDisplay.setPosition(22,15);
		addActor(soundDisplay);
		Assets.getInstance().soundOn = true;
		soundDisplay.addListener(new InputListener(){
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if(Assets.getInstance().soundOn) {
					Assets.getInstance().stopMusic("sfx/bgm.mp3");
					Assets.getInstance().soundOn = false;
					soundDisplay.setTexture(soundOff);
				} else {
					
					Assets.getInstance().soundOn = true;
					Assets.getInstance().playMusic("sfx/bgm.mp3");
					soundDisplay.setTexture(sound);
				}
				return super.touchDown(event, x, y, pointer, button);
			}
		});
		
		window.setVisible(false);
	}

	private void showOption() {
		window.setVisible(true);
	}
	
	private NinePatchButton createNinePatchButton(NinePatch ninepatch,float width,float height) {
		NinePatchButton btn = new NinePatchButton(ninepatch, ninepatch, width, height);
		btn.setTweenManager(manager);
		addActor(btn);
//		btn.addListener(new ActorDragListener());
		return btn;
	}
	
	private void addText(NinePatchButton btn,String text,BitmapFont font) {
		DisplayText display = new DisplayText(text, font);
		display.autoResize = false;
		display.setAlignment(HAlignment.CENTER);
		display.setPreferedWidth(btn.getWidth());
		display.setY(5);
		btn.addActor(display);
		//display.isDebug = true;
	}
	
}
