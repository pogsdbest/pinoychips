package com.quackquack.pinoychips.screen;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Cubic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.utils.DisplayObjectTweenAccessor;
import com.game.framework.utils.DisplayTextTweenAccessor;
import com.game.framework.utils.L;
import com.quackquack.pinoychips.Assets;
import com.quackquack.pinoychips.gameboard.AIPlayer;
import com.quackquack.pinoychips.gameboard.Chip;
import com.quackquack.pinoychips.gameboard.GameBoard;

public class GameScreen extends AbstractScreen {

	private DisplayObject pauseBG;
	private DisplayObject fadeBG;
	private DisplayText messageDisplay;
	private GameBoard gameboard;
	private AIPlayer ai;

	public GameScreen(int bg,int arr,int gameMode) {
		super();
		
		TextureAtlas atlas = Assets.getInstance().get("gfx/assets.pack");
		TextureRegion bgTexture = atlas.findRegion("bg"+(bg+1));
		
		DisplayObject bgDisplay = new DisplayObject(bgTexture);
		addActor(bgDisplay);
		
		gameboard = new GameBoard(arr,gameMode);
		gameboard.setPosition(-60, 625);
		gameboard.setTweenManager(manager);
		gameboard.init();
		gameboard.setGameScreen(this);
		addActor(gameboard);
		
		
		
		pauseBG = new DisplayObject(){
			@Override
			public void draw(Batch batch, float parentAlpha) {
				batch.end();
				Gdx.gl.glEnable(GL20.GL_BLEND);
			    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
				debugRenderer.begin(ShapeType.Filled);
				debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
				debugRenderer.setColor(new Color(0,0,0,getAlpha()));
				debugRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
				debugRenderer.end();
				Gdx.gl.glDisable(GL20.GL_BLEND);
				batch.begin();
				//debugRenderersuper.draw(batch, parentAlpha);
			}
			
		};
		pauseBG.setSize(stage.getWidth(), stage.getHeight());
		pauseBG.setAlpha(0f);
		addActor(pauseBG);
		
		
		
		fadeBG = new DisplayObject(){
			@Override
			public void draw(Batch batch, float parentAlpha) {
				batch.end();
				Gdx.gl.glEnable(GL20.GL_BLEND);
			    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
				debugRenderer.begin(ShapeType.Filled);
				debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
				debugRenderer.setColor(new Color(.153f,.174f,.898f,getAlpha()));
				debugRenderer.rect(0, stage.getHeight()/2 - 40, stage.getWidth(), 80);
				debugRenderer.end();
				Gdx.gl.glDisable(GL20.GL_BLEND);
				batch.begin();
			}
		};
		fadeBG.setAlpha(.5f);
		fadeBG.setVisible(false);
		addActor(fadeBG);
		
		gameboard.createNotification();
		
		BitmapFont font = Assets.getInstance().generateFont(40, Assets.getInstance().getDefaultAllCharSet());
		messageDisplay = new DisplayText("", font);
		messageDisplay.autoResize = false;
		messageDisplay.setAlignment(HAlignment.CENTER);
		messageDisplay.setPreferedWidth(stage.getWidth());
		messageDisplay.setY(stage.getHeight()/2 - 20);
		addActor(messageDisplay);
		
		if(gameMode == PreGameScreen.GAME_MODE_PLAYER_VS_AI) {
			ai = new AIPlayer(gameboard, Chip.Type.BLACK);
		}
		
		initScreen(new ScreenListener() {
			
			@Override
			public void initDone() {
				fadeBG.setVisible(true);
				gameboard.showMessage("Push a total of 800 to win a game.", new InputListener(){
					@Override
					public boolean touchDown(InputEvent event, float x, float y,
							int pointer, int button) {
						gameboard.closeMessage();
						gameboard.closeText.removeListener(this);
						String turn = gameboard.getTurn() == Chip.Type.WHITE ? "WHITE" : "BLACK";
						fade(turn+" TURN",new Callback() {
							
							@Override
							public void done() {
								startTurn();
							}
						});
						return super.touchDown(event, x, y, pointer, button);
					}
				});
				
			}
		});
	}
	
	public void fade(final String message,final Callback callback) {
		fadePauseBG(new Callback() {
			
			@Override
			public void done() {
				fadeBG.setVisible(true);
				messageDisplay.setText(message);
				fadeMessageDisplay(new Callback() {
					
					@Override
					public void done() {
						fadeBG.setVisible(false);
						fadePauseBG(new Callback() {
							
							@Override
							public void done() {
								pauseBG.setVisible(false);
								if(callback!=null) callback.done();
							}
						}, 0f);
					}
				});
			}
		},.75f);
	}
	
	public void fadePauseBG(final Callback callback,final float alpha) {
		pauseBG.setVisible(true);
		Tween.to(pauseBG, DisplayObjectTweenAccessor.ALPHA, 1.2f)
		.target(alpha)
		.ease(Cubic.IN)
		.start(manager)
		.setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				callback.done();
			}
		});
	}
	
	public void fadeMessageDisplay(final Callback callback) {
		messageDisplay.setVisible(true);
		messageDisplay.setColor(new Color(1,1,1,0));
		Tween.to(messageDisplay, DisplayTextTweenAccessor.ALPHA, 2f)
		.target(1f)
		.ease(Cubic.IN)
		.repeatYoyo(1, 2f)
		.start(manager)
		.setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				callback.done();
			}
		});
	}
	
	public void won(final String message) {
		fadePauseBG(new Callback() {
			
			@Override
			public void done() {
				fadeBG.setVisible(true);
				messageDisplay.setText(message);
				Assets.getInstance().playSound("sfx/wongame.mp3");
				fadeMessageDisplay(new Callback() {
					
					@Override
					public void done() {
						switchScreen(new PreGameScreen());
					}
				});
			}
		},.75f);
	}
	
	public interface Callback {
		public void done();
	}
	
	public void startTurn() {
		if(ai!=null) {
			if(ai.type == gameboard.getTurn()) {
				ai.turn();
			}
		}
			
	}
	
	public void showPauseBG() {
		pauseBG.setVisible(true);
	}
	
	public void hidePauseBG() {
		pauseBG.setVisible(false);
	}

}
