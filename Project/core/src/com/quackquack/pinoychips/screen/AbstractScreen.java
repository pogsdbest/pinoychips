package com.quackquack.pinoychips.screen;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.equations.Quad;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.game.framework.display.DisplayObject;
import com.game.framework.display.DisplayText;
import com.game.framework.manager.ScreenManager;
import com.game.framework.utils.ActorTweenAccessor;
import com.game.framework.utils.BackKeyCatcher;
import com.game.framework.utils.DisplayObjectTweenAccessor;
import com.game.framework.utils.DisplayTextTweenAccessor;
import com.game.framework.utils.L;
import com.quackquack.pinoychips.Config;

public class AbstractScreen implements Screen{

	protected Stage stage;
	protected TweenManager manager;
	protected OrthographicCamera camera;
	protected SpriteBatch batch;
	private BitmapFont font;

	public AbstractScreen() {
		
		stage = new Stage(new FitViewport(Config.SCREEN_WIDTH, Config.SCREEN_HEIGHT));
		
		camera = new OrthographicCamera(Config.SCREEN_WIDTH,Config.SCREEN_HEIGHT);
		camera.position.set(Config.SCREEN_WIDTH / 2, Config.SCREEN_HEIGHT / 2 , 0);
		camera.zoom = 1;
		camera.update();
		batch = new SpriteBatch();
		batch.setProjectionMatrix(camera.combined);
		
		Gdx.input.setInputProcessor(stage);
		manager = new TweenManager();
		Tween.registerAccessor(Actor.class, new ActorTweenAccessor());
		Tween.registerAccessor(DisplayText.class, new DisplayTextTweenAccessor());
		Tween.registerAccessor(DisplayObject.class, new DisplayObjectTweenAccessor());
		
		font = new BitmapFont();
		Gdx.input.setCatchBackKey(true);
		stage.addListener(new InputListener(){
			@Override
			public boolean keyDown(InputEvent event, int keycode) {
				if(keycode == Keys.BACK){
					L.wtf("back key pressed");
					BackKeyCatcher.getInstance().backPressed();
		        }
				return super.keyDown(event, keycode);
			}
		});
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glEnable(GL20.GL_BLEND);
	    stage.act(Gdx.graphics.getDeltaTime());
	    update();
	    
	    stage.draw();
	    
	    batch.begin();
//	    font.draw(batch, "Delta Time : " + Gdx.graphics.getDeltaTime() , 20, stage.getHeight() - 20);
	    //font.draw(batch, "Scale Ratio : x : " + getScaleRatio().x + " y : "+ getScaleRatio().y , 20, stage.getHeight() - 40);
	    draw(batch);
	    batch.end();
	    
	    manager.update(delta);
	    camera.update();
	}
	
	public Vector2 getScaleRatio() {
		float scaleX = Config.SCREEN_WIDTH/Gdx.graphics.getWidth();
		float scaleY = Config.SCREEN_HEIGHT/Gdx.graphics.getHeight();
		return new Vector2(scaleX,scaleY);
	}
	
	public void update() {
	}

	protected void draw(SpriteBatch batch) {
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		stage.dispose();
	}
	
	public void addActor(Actor actor) {
		stage.addActor(actor);
	}
	
	public TweenManager getManager() {
		return manager;
	}
	
	public void switchScreen(final AbstractScreen screen ) {
		
		DisplayObject bg = new DisplayObject(){
			@Override
			public void draw(Batch batch, float parentAlpha) {
				batch.end();
				Gdx.gl.glEnable(GL20.GL_BLEND);
			    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
				debugRenderer.begin(ShapeType.Filled);
				debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
				debugRenderer.setColor(new Color(0,0,0,getAlpha()));
				debugRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
				debugRenderer.end();
				Gdx.gl.glDisable(GL20.GL_BLEND);
				batch.begin();
				//debugRenderersuper.draw(batch, parentAlpha);
			}
		};
		bg.setAlpha(0);
		Tween.to(bg, DisplayObjectTweenAccessor.ALPHA, 1f)
	    .target(1f)
	    .ease(Quad.OUT)
	    .setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				ScreenManager.getInstance().setScreen(screen);
			}
		})
	    .start(manager);
		addActor(bg);
	}
	
	public void initScreen() {
		initScreen(null);
	}
	
	public void initScreen(final ScreenListener listener) {
		final DisplayObject bg = new DisplayObject(){
			@Override
			public void draw(Batch batch, float parentAlpha) {
				batch.end();
				Gdx.gl.glEnable(GL20.GL_BLEND);
			    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
				debugRenderer.begin(ShapeType.Filled);
				debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
				debugRenderer.setColor(new Color(0,0,0,getAlpha()));
				debugRenderer.rect(0, 0, stage.getWidth(), stage.getHeight());
				debugRenderer.end();
				Gdx.gl.glDisable(GL20.GL_BLEND);
				batch.begin();
				//debugRenderersuper.draw(batch, parentAlpha);
			}
			
		};
		Tween.to(bg, DisplayObjectTweenAccessor.ALPHA, 1f)
	    .target(0f)
	    .ease(Quad.OUT)
	    .setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				bg.remove();
				if(listener!=null) listener.initDone();
			}
		})
	    .start(manager);
		addActor(bg);
	}
	
}
