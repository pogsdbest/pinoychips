package com.quackquack.pinoychips;

import com.badlogic.gdx.Game;
import com.game.framework.manager.ScreenManager;
import com.quackquack.pinoychips.screen.LoadingScreen;

public class PinoyChipsGame extends Game {
	@Override
	public void create() {
		Assets.getInstance().init();
		ScreenManager.getInstance().init(this);
		
		ScreenManager.getInstance().setScreen(new LoadingScreen());
	}
	
	@Override
	public void dispose() {
		
		super.dispose();
	}
	
	@Override
	public void pause() {
		
		super.pause();
	}
}
