package com.quackquack.pinoychips;

import java.util.ArrayList;
import java.util.HashSet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.game.framework.utils.L;

public class Assets {

	private static Assets instance;
	public AssetManager manager;
	private FreeTypeFontGenerator defaultFontGenerator;
	public String charSet;
	private Music currentMusic;
	private ArrayList<Sound> sounds;
	private ArrayList<Music> music;
	public boolean soundOn;

	public static Assets getInstance() {
		if (instance == null) {
			instance = new Assets();
		}
		return instance;
	}

	public void init() {
		sounds = new ArrayList<Sound>();
		music = new ArrayList<Music>();
		manager = new AssetManager();
		
		manager.load("gfx/assets.pack",TextureAtlas.class);
		
		manager.load("sfx/bgm.mp3",Music.class);
		manager.load("sfx/wongame.mp3", Sound.class);
		manager.load("sfx/lostchip.mp3", Sound.class);
		manager.load("sfx/movechip.mp3", Sound.class);
		manager.load("sfx/touchchip.mp3", Sound.class);
		
		manager.load("data/particle/destroy.p",ParticleEffect.class);
		
		defaultFontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("data/font/debussy.ttf"));
		charSet = getDefaultAllCharSet();
	}
	
	public <T> T get(String name) {
		if (!manager.isLoaded(name)) {
			L.e(name + "is not loaded");
		}
		return manager.get(name);
	}
	
	public void dispose() {
		for(Sound sound : sounds) {
			sound.dispose();
		}
		
		for(Music m : music) {
			m.stop();
			m.dispose();
		}
		manager.clear();
	}
	
	public BitmapFont generateFont(int size,String string) {
		BitmapFont font = defaultFontGenerator.generateFont(size, getUniqueChars(string), false);
		return font;
	}
	
	public String getDefaultAllCharSet() {
		String charSet = "";
		for (int c=1; c<128; c++) { 
			charSet += ((char)c); 
		}
		return charSet;
	}
	
	public String getUniqueChars(String s) {
		String string = s.replace("\\n","");
		HashSet<String> set = new HashSet<String>();
		for(int i=0;i<string.length();i++) {
			set.add(string.charAt(i)+"");
		}
		String out = "";
		for(String value : set) {
			out += value;
		}
		return out;
	}
	
	public void playMusic(String name) {
		if(!soundOn) return;
		Music m = get(name);
		if(m==null) return;
		if(!music.contains(m)) {
			music.add(m);
		}
		if(m.isPlaying()) return;
		m.play();
	}
	
	public void stopMusic(String name) {
		Music m = get(name);
		if(m!=null)m.stop();
	}
	
	public void playSound(String name) {
		if(!soundOn) return;
		Sound sound = get(name);
		sound.play();
	}

}
