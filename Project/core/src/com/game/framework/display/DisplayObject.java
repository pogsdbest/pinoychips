package com.game.framework.display;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Group;

public class DisplayObject extends Group {

	private TextureRegion texture;
	protected boolean flipX;
	protected boolean flipY;
	private float alpha;
	public boolean isDebug = false;
	protected ShapeRenderer debugRenderer= new ShapeRenderer();
	//public boolean visible;
	private TweenManager manager;
	
	public DisplayObject() {
		// TODO Auto-generated constructor stub
		this(null);
	}
	public DisplayObject(TextureRegion texture) {
		super();
		
		init(texture);
	}
	
	public void init(TextureRegion texture) {
		this.setTexture(texture);
		flipX = false;
		flipY = false;
		setAlpha(1f);
		setVisible(true);
		if(texture!=null) {
			setSize(texture.getRegionWidth(), texture.getRegionHeight());
		}
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		if(texture!=null) {
			batch.setColor(getColor().r, getColor().g, getColor().b, getAlpha());
			batch.draw(getTexture().getTexture(), getX(), getY(), getOriginX(), getOriginY(),
					getWidth(), getHeight(), getScaleX(), getScaleY(),
					getRotation(), getTexture().getRegionX(), getTexture().getRegionY(),
					getTexture().getRegionWidth(), getTexture().getRegionHeight(), flipX,
					flipY);
			batch.setColor(1f, 1f, 1f, 1f);
			
			if(isDebug) {
				batch.end();
				debugRenderer.begin(ShapeType.Line);
				debugRenderer.setColor(Color.BLUE);
				debugRenderer.rect(getX()+getParent().getX(), getY()+getParent().getY(), getWidth(),getHeight());
				debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
				debugRenderer.end();
				batch.begin();
			}
		}
		
		
		super.draw(batch, parentAlpha);
	}
	
	@Override
	public void act(float delta) {
		// TODO Auto-generated method stub
		super.act(delta);
	}
	
	public Rectangle getBounds() {
		if(texture==null) {
			return new Rectangle();
		}
		return new Rectangle(getX(),getY(),getWidth(),getHeight());
	}

	public TextureRegion getTexture() {
		return texture;
	}

	public void setTexture(TextureRegion texture) {
		this.texture = texture;
	}
	public float getAlpha() {
		return alpha;
	}
	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}
	
	public void setFlipX(boolean value) {
		flipX = value;
	}
	
	public boolean isFlipX() {
		return flipX;
	}
	
	public void setTweenManager(TweenManager manager) {
		this.manager = manager;
		
	}
	
	public TweenManager getTweenManager() {
		return this.manager;
	}
	
}
