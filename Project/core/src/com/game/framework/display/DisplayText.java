package com.game.framework.display;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class DisplayText extends Actor{
	
	private String text;
	private BitmapFont font;
	private HAlignment alignment;
	private Batch batch;
	private float width;
	private float height;
	public boolean autoResize;
	private Color color;
	private float preferedWidth;
	private ShapeRenderer debugRenderer;
	public boolean isDebug;

	public DisplayText() {
		this("",new BitmapFont());
		// TODO Auto-generated constructor stub
	}
	
	public DisplayText(String text) {
		this(text,new BitmapFont());
		// TODO Auto-generated constructor stub
	}
	
	public DisplayText(String text,BitmapFont font) {
		super();
		this.text = text;
		this.font = font;
		setPreferedWidth(font.getMultiLineBounds(text).width);
		setWidth(getTextWidth());
		setHeight(getTextHeight());
		this.setAlignment(HAlignment.LEFT);
		this.autoResize = true;
		this.color = font.getColor();
		// TODO Auto-generated constructor stub
		
		debugRenderer = new ShapeRenderer();
		isDebug = false;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		// TODO Auto-generated method stub
		this.batch = batch;
		if(autoResize) {
			setWidth(getTextWidth());
			setHeight(getTextHeight());
		}
		float dx = getX();
		float dy = getY();
		
		font.setColor(new Color(getColor().r,getColor().g,getColor().b,getColor().a));
		font.drawWrapped(batch, text, dx, dy+font.getWrappedBounds(text,preferedWidth).height,getPreferedWidth() ,getAlignment());
		batch.end();
		if(isDebug) {
			Vector2 local = new Vector2(dx, dy);
			debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
			debugRenderer.begin(ShapeType.Line);
			debugRenderer.setColor(Color.BLUE);
			debugRenderer.rect(local.x+getParent().getX(),local.y+getParent().getY(),getPreferedWidth(),getHeight());
			
			
			debugRenderer.end();
		}
		batch.begin();
		super.draw(batch, parentAlpha);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		
	}

	public BitmapFont getFont() {
		return font;
	}

	public void setFont(BitmapFont font) {
		this.font = font;
	}
	
	@Override
	public void setSize(float width, float height) {
		// TODO Auto-generated method stub
		setWidth(width);
		setHeight(height);
		super.setSize(width, height);
	}
	@Override
	public float getWidth() {
		// TODO Auto-generated method stub
		return width;
	}
	
	@Override
	public void setWidth(float width) {
		// TODO Auto-generated method stub
		this.width = width;
		super.setWidth(width);
	}
	
	@Override
	public float getHeight() {
		// TODO Auto-generated method stub
		return height;
	}
	
	@Override
	public void setHeight(float height) {
		// TODO Auto-generated method stub
		this.height = height;
		super.setHeight(height);
	}

	public HAlignment getAlignment() {
		return alignment;
	}

	public void setAlignment(HAlignment alignment) {
		this.alignment = alignment;
	}
	
	public float getTextWidth() { 
		return font.getWrappedBounds(text,preferedWidth).width;
	}
	
	public float getTextHeight() {
		return font.getWrappedBounds(text, preferedWidth).height;
	}

	public float getPreferedWidth() {
		return preferedWidth;
	}

	public void setPreferedWidth(float preferedWidth) {
		this.preferedWidth = preferedWidth;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(getX(),getY(),getPreferedWidth(),getHeight());
	}
	
	public void setPreferredWidthInBounds(float value) {
		setPreferedWidth(value);
		setWidth(value);
	}

}
