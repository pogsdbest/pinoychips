package com.game.framework.display.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.game.framework.display.DisplayObject;

public class NinePatchDisplay extends DisplayObject {
	
	private NinePatch ninePatch;
	private Color ninePatchColor;

	public NinePatchDisplay(NinePatch ninePatch,float width,float height) {
		super();
		this.ninePatch = ninePatch;
		setSize(width, height);
		ninePatchColor = new Color();
	}
	
	@Override
	public Actor hit(float x, float y, boolean touchable) {
		x = x + ((getWidth()-getScaleWidth())/2);
		y = y + ((getHeight()-getScaleHeight())/2);
		if (touchable && this.getTouchable() != Touchable.enabled) return null;
		return x >= 0 && x < getScaleWidth() && y >= 0 && y < getScaleHeight() ? this : null;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		
		float x = getX()+((getWidth()-getScaleWidth())/2);
		float y = getY()+((getHeight()-getScaleHeight())/2);
		
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		
		batch.setColor(getColor());
		ninePatch.setColor(ninePatchColor.set(getColor().r, getColor().g, getColor().b, getAlpha()));
		ninePatch.draw(batch, x, y, getScaleWidth(), getScaleHeight());
		Gdx.gl.glDisable(GL20.GL_BLEND);
		
		if(isDebug) {
			batch.end();
			debugRenderer.begin(ShapeType.Line);
			debugRenderer.setColor(Color.BLUE);
			debugRenderer.rect(x, y, getScaleWidth(),getScaleHeight());
			debugRenderer.setProjectionMatrix(batch.getProjectionMatrix());
			debugRenderer.end();
			batch.begin();
		}
		super.draw(batch, parentAlpha);
	}
	
	public float getScaleWidth() {
		return super.getWidth()*getScaleX();
	}
	
	public float getScaleHeight() {
		return super.getHeight()*getScaleY();
	}

}
