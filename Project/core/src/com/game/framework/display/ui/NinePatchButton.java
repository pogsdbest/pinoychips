package com.game.framework.display.ui;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Cubic;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.game.framework.display.DisplayObject;
import com.game.framework.listeners.ActorClickListener;
import com.game.framework.utils.ActorTweenAccessor;

public class NinePatchButton extends NinePatchDisplay {
	
	private NinePatch buttonUp;
	private NinePatch buttonDown;
	private NinePatch current;
	private ActorClickListener listener;
	private boolean isClickable;

	public NinePatchButton(NinePatch buttonUp,NinePatch buttonDown,float width,float height) {
		super(buttonUp,width,height);
		this.buttonUp = buttonUp;
		this.buttonDown = buttonDown;
		this.current = buttonUp;
		this.isClickable = true;
		setOrigin(getWidth()/2,getHeight()/2);
		//isDebug = true;
		
		addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
			}
			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				buttonClicked();
				return super.touchDown(event, x, y, pointer, button);
			}
		});
	}
	
	
	
	public void setClickListener(ActorClickListener listener) {
		this.listener = listener;
	}
	
	
	
	public void buttonClicked() {
		if(!isClickable) return;
		tweenClicked();
		
	}

	private void tweenClicked() {
		isClickable = false;
		final DisplayObject displayObject = this;
		Tween.to(this, ActorTweenAccessor.SCALE_XY, .25f)
		.target(.75f,.75f)
	    .ease(Cubic.IN)
	    .repeatYoyo(1, 0f)
	    .start(getTweenManager())
	    .setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int arg0, BaseTween<?> arg1) {
				isClickable = true;
				if(listener!=null) {
					
					listener.clicked(displayObject);
				}
			}
		});
	}
	
	

}
