package com.game.framework.display;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.game.framework.listeners.ParticleDisplayListener;

public class ParticleDisplay extends DisplayObject {
	
	private ParticleEffect effect;
	private ParticleDisplayListener listener;
	private boolean isCompleted;
	
	public ParticleDisplay(ParticleEffect effect) {
		super(null);
		
		this.effect = effect;
		isCompleted = false;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		effect.draw(batch);
		
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);
		effect.setPosition(getX(), getY());
		effect.update(delta);
		if(effect.isComplete() && !isCompleted) {
			if(listener!=null) {
				listener.complete();
				isCompleted = true;
			}
		}
	}

	public void reset() {
		effect.reset();
		isCompleted = false;
	}
	
	public void setCompleteListener(ParticleDisplayListener listener) {
		this.listener = listener;
		
	}

}
