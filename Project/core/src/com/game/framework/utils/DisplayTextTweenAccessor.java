package com.game.framework.utils;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenAccessor;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.equations.Cubic;

import com.badlogic.gdx.graphics.Color;
import com.game.framework.display.DisplayText;

public class DisplayTextTweenAccessor implements TweenAccessor<DisplayText> {

	public static final int ALPHA = 201;

	@Override
	public void setValues(DisplayText target, int tweenType, float[] newValues) {
		switch (tweenType) {
		case ALPHA:
			Color color = target.getColor();
			target.setColor(color.r,color.g,color.b,newValues[0]);
			break;
		default:
			assert false;
			break;
		}

	}

	@Override
	public int getValues(DisplayText target, int tweenType, float[] returnValues) {
		switch (tweenType) {
		case ALPHA:
			
			returnValues[0] = target.getColor().a;
			return 1;
		default:
			assert false;
			return -1;
		}
	}
	
	
	
	public static void putFadeInFadeOut(TweenManager manager,DisplayText text) {
		FadeInFadeOut f = new FadeInFadeOut(manager);
		f.fadeOut(text);
	}
}


class FadeInFadeOut {
	
	private TweenManager manager;

	public FadeInFadeOut(TweenManager manager) {
		this.manager = manager;
		
	}
	
	public void fadeIn(final DisplayText text) {
		text.setColor(text.getColor().r,text.getColor().g,text.getColor().b,0);
		Tween.to(text, DisplayTextTweenAccessor.ALPHA, 1f)
	    .target(1)
	    .ease(Cubic.OUT)
	    .start(manager).setCallback(new TweenCallback() {
	    	@Override
			public void onEvent(int arg, BaseTween<?> arg1) {
				// TODO Auto-generated method stub
				if(arg == TweenCallback.COMPLETE) {
					fadeOut(text);
				}
			}
	    });
	}
	
	public void fadeOut(final DisplayText text) {
		text.setColor(text.getColor().r,text.getColor().g,text.getColor().b,1);
		Tween.to(text, DisplayTextTweenAccessor.ALPHA, 1f)
	    .target(0)
	    .ease(Cubic.OUT)
	    .start(manager).setCallback(new TweenCallback() {
	    	@Override
			public void onEvent(int arg, BaseTween<?> arg1) {
				// TODO Auto-generated method stub
				if(arg == TweenCallback.COMPLETE) {
					fadeIn(text);
				}
			}
	    });
	}
}
