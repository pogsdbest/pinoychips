package com.game.framework.utils;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.game.framework.display.DisplayObject;

public class DisplayObjectTweenAccessor extends ActorTweenAccessor {
	
	public static final int ALPHA = 101;
	
	public void setValues(DisplayObject target, int tweenType, float[] newValues) {
		switch (tweenType) {
		case ALPHA:
			target.setAlpha(newValues[0]);
			break;
		}
		
		super.setValues(target, tweenType, newValues);
	}
	
	public int getValues(DisplayObject target, int tweenType, float[] returnValues) {
		switch (tweenType) {
		case ALPHA:
			returnValues[0] = target.getAlpha();
			return 1;
		}
		
		return super.getValues(target, tweenType, returnValues);
	}
	
	@Override
	public int getValues(Actor target, int tweenType, float[] returnValues) {
		// TODO Auto-generated method stub
		return getValues((DisplayObject)target, tweenType, returnValues);
	}
	
	@Override
	public void setValues(Actor target, int tweenType, float[] newValues) {
		// TODO Auto-generated method stub
		setValues((DisplayObject)target, tweenType, newValues);
	}

}
