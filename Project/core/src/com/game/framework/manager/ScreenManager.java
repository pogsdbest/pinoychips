package com.game.framework.manager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;

public class ScreenManager {

	private static ScreenManager instance;
	private Game game;
	
	public static ScreenManager getInstance() {
		if(instance == null) {
			instance = new ScreenManager();
		}
		return instance;
	}
	
	public void init(Game game) {
		this.game = game;
	}
	
	public void setScreen(Screen screen) {
		game.setScreen(screen);
	}
	
	public Game getGame() {
		return game;
	}
}
